<?php

return [

    'header' => [
        'Find Work' => 'Trouver un travail',
        'Browse Jobs' => 'Parcourir les emplois',
        'Browse Companies' => 'Parcourir les entreprises',
        'For Employers' => 'Pour les employeurs',
        'Find a Freelancer' => 'Trouver un candidat',
        'Post a Job' => 'Publier une offre d\'emploi',
        'How it works' => 'Comment ça marche',
        'Contact' => 'Contact',
        'Log In / Register' => 'Connexion / Inscription',
    ],

    'footer' => [
        'For Candidates' => 'Pour les candidats',
        'Browse Jobs' => 'Parcourir les offres d\'emploi',
        'Browse Companies' => 'Parcourir les entreprises',
        'Add Resume' => 'Ajouter un CV',
        'My Bookmarks' => 'Mes favoris',
        'For Employers' => 'Pour les employeurs',
        'Find a Freelancer' => 'Trouver un candidat',
        'Post a Job' => 'Publier une offre d\'emploi',
        'Helpful Links' => 'Liens utiles',
        'How it works' => 'Comment ça marche',
        'Contact' => 'Contact',
        'Privacy Policy' => 'Politique de confidentialité',
        'Terms of Use' => 'Conditions d\'utilisation',
        'Account' => 'Compte',
        'Mon compte' => 'Mon compte',
        'Log In' => 'Connexion',
        'Subscribe' => 'Inscrivez-vous à notre newsletter',
        'Subscribe text' => 'Actualités hebdomadaires, analyses et conseils de pointe sur la recherche d\'emploi.',
        'Subscribe placeholder' => 'Entrez votre adresse e-mail'
    ],

    'Routes' => [
        'Dashboard' => 'Tableau de bord',
        'Settings' => 'Paramètres'
    ],

    'Employer' => 'Employeur',
    'Freelancer' => 'Candidat',

    'types' => [
        'Full Time' => 'Temps plein',
        'Freelance' => 'Freelance',
        'Part Time' => 'Temps partiel',
        'Internship' => 'Stage',
        'Temporary' => 'Temporaire'
    ],

    'categories' => [
        'Agriculture, Food and Natural Resources' => 'Agriculture, alimentation et ressources naturelles',
        'Architecture and Construction' => 'Architecture et construction',
        'Arts, Audio/Video Technology and Communications' => 'Arts, technologie audio/vidéo et communications',
        'Business Management and Administration' => 'Gestion et administration des affaires',
        'Education and Training' => 'Education et formation',
        'Finance' => 'Finance',
        'Government and Public Administration' => 'Gouvernement et administration publique',
        'Health Science' => 'Santé',
        'Hospitality and Tourism' => 'Hôtellerie et Tourisme',
        'Human Services' => 'Services à la personne',
        'Information Technology' => 'Technologie de l\'information',
        'Law, Public Safety, Corrections and Security' => 'Droit, La sécurité publique, Corrections et sécurité',
        'Manufacturing' => 'Fabrication',
        'Marketing, Sales and Service' => 'Marketing, Ventes et Service',
        'Science, Technology, Engineering and Mathematics' => 'Science, technologie, ingénierie et mathématiques',
        'Transportation, Distribution & Logistics' => 'Transport, Distribution & Logistique',
    ],

];
