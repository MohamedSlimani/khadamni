<?php

return [
    'welcome' => [
        'hero' => [
            'title' => 'Engagez des experts en informatique ou soyez embauché pour n\'importe quel travail, à tout moment.',
            'text' => 'Des milliers de petites entreprises utilisent <strong class = "color">:name </strong> pour transformer leurs idées en réalité.',
        ],
        'stats' => [
            'jobs' => 'Emplois publiés',
            'freelancers' => 'Candidats',
            'companies' => 'Entreprises'
        ],
        'Latest Jobs' => 'offres d\'emploi récentes',
        'Browse All Jobs' => 'Parcourir toutes les offres d\'emploi'
    ],

    'jobs' => [
        'index' => [
            'Search Results' => 'Résultats de la recherche',
            'Show Filters' => 'Afficher les filtres',
            'Hide Filters' => 'Masquer les filtres',
        ],

        'show' => [
            'About the Employer' => 'À propos de l\'employeur',
            'Salary' => 'Salaire',
            'Job Description' => 'Description du poste',
            'Location' => 'Localisation',
            'Similar Jobs' => 'Emplois similaires',
            'Job Summary' => 'Résumé sur l\'offre d\'emploi',
            'Job Type' => 'Type de poste',
            'Date Posted' => 'Date de publication',
            'Bookmark or Share' => 'Marquer ou Partager',
            'Interesting? Share It!' => 'Intéressant? <strong> Partagez-le! </strong> ',
            'Copied to clipboard!' => 'Copié dans le presse-papier!'
        ]
    ],

    'companies' => [
        'index' => [
            'Browse Companies' => 'Parcourir les entreprises',
            'Home' => 'Accueil',
            'Companies' => 'Entreprises',
            'Nothing found' => 'Rien trouvé ...'
        ],

        'show' => [
            'About Company' => 'À propos de l\'entreprise',
            'Open Positions' => 'Postes ouverts',
            'Location' => 'Localisation',
            'Social Profiles' => 'Profils sociaux',
            'Bookmark or Share' => 'Marquer ou Partager',
            'Interesting? Share It!' => 'Intéressant? <strong> Partagez-le! </strong> ',
            'Copied to clipboard!' => 'Copié dans le presse-papier!'
        ]
    ],

    'users' => [
        'index' => [
            'Search Results' => 'Résultats de la recherche',
            'Location' => 'Localisation',
            'Nothing found' => 'Rien trouvé ...',
            'Location not available' => 'Localisation non disponible',
            'View Profile' => 'Voir le profil'
        ],

        'show' => [
            'About Me' => 'À propos de moi',
            'Skills' => 'Compétences',
            'Attachments' => 'Pièces jointes',
            'Social Profiles' => 'Profils sociaux',
            'Bookmark or Share' => 'Marquer ou Partager',
            'Interesting? Share It!' => 'Intéressant? <strong> Partagez-le! </strong> ',
            'Copied to clipboard!' => 'Copié dans le presse-papier!'
        ]
    ],

    'how-it-works' => [
        'How does it work' => 'Comment ça marche?',
        'Home' => 'Accueil',
    ],

    'Contact' => [
        'Contact' => 'Contact',
        'Home' => 'Accueil',
        'Our Office' => 'Notre bureau',
        'Any questions' => 'Des questions? N\'hésitez pas à nous contacter!',

         'form' => [
             'Your Name' => 'Votre nom',
             'Email Address' => 'Adresse e-mail',
             'Subject' => 'Sujet',
             'Message' => 'Message',
             'Submit Message' => 'Envoyer Le Message'
         ]
     ]
];
