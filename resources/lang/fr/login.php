<?php

return [
    'Log In' => 'Connexion',
    'Home' => 'Accueil',
    'We\'re glad to see you again!' => 'Nous sommes heureux de vous revoir!',
    'Don\'t have an account? ' => 'Vous n\'avez pas de compte? ',
    'Sign Up!' => 'Inscrivez-vous!',

    'form' => [
        'Email' => 'Email',
        'Password' => 'Mot de passe',
        'Remember Me' => 'Se souvenir de moi',
        'Full name' => 'Nom complet',
        'Repeat Password' => 'Répéter le mot de passe'
    ],

    'Log In via Facebook' => 'Se connecter via Facebook',
    'Log In via Google' => 'Se connecter via Google',

    'Register via Facebook' => 'S\'inscrire via Facebook',
    'Register via Google' => 'S\'inscrire via Google',

    'Register' => 'S\'inscrire',
    "Let's create your account!" => 'Créons votre compte!',
    'Already have an account?' => 'Vous avez déjà un compte?',
    'Log In!' => 'Connectez-vous!',

    'or' => 'ou'

];
