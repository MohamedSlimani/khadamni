<?php

return [
    'sidebar' => [
        'Dashboard Navigation' => 'Navigation dans le dashboard',
        'Organize and Manage' => 'Organiser et gérer',
        'Bookmarks' => 'Favoris',
        'Jobs' => 'Emplois',
        'Manage Jobs' => 'Gérer les offres d\'emploi',
        'Post a Job' => 'Publier une offre d\'emploi',
        'Jobs Applications' => 'Mes postulations',
        'Account' => 'Compte',
        'Settings' => 'Paramètres',
        'Update Account' => 'Mettre à jour le compte',
        'Update Password' => 'Mettre à jour le mot de passe',
        'Logout' => 'Déconnexion'
    ],

    'settings' => [
        'account' => [
            'Update Account' => 'Mettre à jour le compte',
            'Dashboard' => 'Tableau de bord',
            'Settings' => 'Paramètres',

            'form' => [
                'My Account' => 'Mon compte',
                'Change Avatar' => 'Changer d\'avatar',
                'Full Name' => 'Nom complet',
                'Email' => 'E-mail',
                'Account Type' => 'Type de compte',
                'Employer' => 'Employeur',
                'Preview' => 'Aperçu',
                'Freelancer' => 'Freelancer',
                'Phone' => 'Téléphone',
                'Details' => 'Détails',
                'Skills' => 'Compétences',
                'Add up to 10 skills' => 'Ajouter jusqu\'à 10 compétences',
                'Title' => 'Fonction',
                'Job Title' => 'Nom poste',
                'Introduce Yourself' => 'Présentez-vous',
                'Save Changes' => 'Enregistrer les modifications'
            ]
        ],

        'password' => [
            'Update password' => 'Mettre à jour le mot de passe',
            'Dashboard' => 'Tableau de bord',
            'Settings' => 'Paramètres',

            'form' => [
                'Password & Security' => 'Mot de passe et sécurité',
                'Current Password' => 'Mot de passe actuel',
                'New Password' => 'Nouveau mot de passe',
                'Repeat New Password' => 'Répéter le nouveau mot de passe',
                'Save Changes' => 'Enregistrer les modifications'
            ]
        ],

        'candidates' => [
            'index' => [
                'Applications' => 'Postulations',
                'Dashboard' => 'Tableau de bord',
                'My Job Applications' => 'Mes postulations',
                'Nothing found' => 'Rien trouvé ...'
            ],


            'edit' => [
                'Applications' => 'Applications',
                'Dashboard' => 'Tableau de bord',
                'Details' => 'Détails',
                'optional' => 'facultatif',
                'Experience' => 'Expérience',
                'years' => 'années',
                'Detials about your experience' => 'Détails sur votre expérience',
                'Save Changes' => 'Enregistrer les modifications'
            ]
        ],

        'bookmarks' => [
            'index' => [
                'Bookmarks' => 'Favoris',
                'Dashboard' => 'Tableau de bord',
                'Bookmarked Jobs' => 'Emplois dans les favoris',
                'Bookmarked Companies' => 'Entreprises mises en favoris',
                'Bookmarked Freelancers' => 'Freelancers mis en favori'
            ]
        ],

        'jobs' => [
            'index' => [
                'Dashboard' => 'Tableau de bord',
                'Manage Jobs' => 'Gérer les offres d\'emplois',
                'My Job Listings' => 'Mes offres d\'emploi publiées',
                'Manage Candidates' => 'Gérer les candidatures',
                'Edit' => 'Modifier',
                'Remove' => 'Supprimer'
            ],

            'edit' => [
                'Dashboard' => 'Tableau de bord',
                'Manage Jobs' => 'Gérer les offres d\'emplois',
                'Job Edit Form' => 'Formulaire de modification de poste',
                'Job Title' => 'Titre du poste',
                'Salary' => 'Salaire <span> (optionnel) </span>',
                'Job Type' => 'Type du poste',
                'Job Category' => 'Domaine',
                'Job Description' => 'Description du poste',
                'Save' => 'Enregistrer'
            ],

            'create' => [
                'Dashboard' => 'Tableau de bord',
                'Post a Job' => 'Publier une offre d\'emploi',
                'Job Submission Form' => 'Formulaire de soumission d\'emploi',
                'Job Title' => 'Titre du poste',
                'Salary' => 'Salaire <span> (optionnel) </span>',
                'Job Type' => 'Type du poste',
                'Job Category' => 'Domaine',
                'Job Description' => 'Description du poste',
                'Save' => 'Enregistrer'
            ],
        ]
    ]
];
