<?php

return [
    'welcome-job-search' => [
        'Where?' => 'Où?',
        'What job you want?' => 'Quel travail tu veux?',
        'Job Title or Keywords' => 'Intitulé du poste ou mots clés',
        'Search' => 'Rechercher',
        'Jobs near you' => 'Emplois près de chez vous',
        'Not Available' => 'Non disponible'
    ],

    'location' => [
        'Location' => 'Emplacement',
        'Not Available' => 'Non disponible'
    ],

    'attachments' => [
        'Attachments' => 'Pièces jointes',
        'optional' => 'facultatif',
        'Upload Files' => 'Upload des fichiers',
        'Maximum file size: 10 MB' => 'Taille maximale du fichier: 10 Mo',
        'Uploading....' => 'Uploading ....',
    ],

    'bookmarks' => [
        'Bookmark' => 'Signet',
        'Bookmarked' => 'Ajouter aux favoris',
        'Bookmark deleted' => 'Signet supprimé',
        'The bookmark was not deleted' => 'Le signet n\'a pas été supprimé',
        'Bookmark added' => 'Signet ajouté',
        'The bookmark was not added' => 'Le signet n\'a pas été ajouté'
    ],

    'search-location' => [
        'Location' => 'Emplacement'
    ],

    'sort-by' => [
        'Sort by:' => 'Trier par:',
        'Relevance' => 'Pertinence',
        'Newest' => 'Plus récente',
        'Oldest' => 'Plus ancien'
    ]
];
