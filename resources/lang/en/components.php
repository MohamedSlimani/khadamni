<?php

return [
    'welcome-job-search' => [
        'Where?' => 'Where?',
        'What job you want?' => 'What job you want?',
        'Job Title or Keywords' => 'Job Title or Keywords',
        'Search' => 'Search',
        'Jobs near you' => 'Jobs near you',
        'Not Available' => 'Not Available'
    ],

    'location' => [
        'Location' => 'Location',
        'Not Available' => 'Not Available'
    ],

    'attachments' => [
        'Attachments' => 'Attachments',
        'optional' => 'optional',
        'Upload Files' => 'Upload Files',
        'Maximum file size: 10 MB' => 'Maximum file size: 10 MB',
        'Uploading....' => 'Uploading....',
    ],

    'bookmarks' => [
        'Bookmark' => 'Bookmark',
        'Bookmarked' => 'Bookmarked',
        'Bookmark deleted' => 'Bookmark deleted',
        'The bookmark was not  deleted' => 'The bookmark was not  deleted',
        'Bookmark added' => 'Bookmark added',
        'The bookmark was not added' => 'The bookmark was not  added'
    ],

    'search-location' => [
        'Location' => 'Location'
    ],

    'sort-by' => [
        'Sort by:' => 'Sort by:',
        'Relevance' => 'Relevance',
        'Newest' => 'Newest',
        'Oldest' => 'Oldest'
    ]
];
