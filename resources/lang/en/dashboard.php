<?php

return [
    'sidebar' => [
        'Dashboard Navigation' => 'Dashboard Navigation',
        'Organize and Manage' => 'Organize and Manage',
        'Bookmarks' => 'Bookmarks',
        'Jobs' => 'Jobs',
        'Manage Jobs' => 'Manage Jobs',
        'Post a Job' => 'Post a Job',
        'Jobs Applications' => 'Jobs Applications',
        'Account' => 'Account',
        'Settings' => 'Settings',
        'Update Account' => 'Update Account',
        'Update Password' => 'Update Password',
        'Logout' => 'Logout'
    ],

    'settings' => [
        'account' => [
            'Update Account' => 'Update Account',
            'Dashboard' => 'Dashboard',
            'Settings' => 'Settings',

            'form' => [
                'My Account' => 'Mon comptet',
                'Change Avatar' => 'Change Avatar',
                'Full Name' => 'Full Name',
                'Email' => 'Email',
                'Account Type' => 'Account Type',
                'Employer' => 'Employer',
                'Preview' => 'Preview',
                'Freelancer' => 'Freelancer',
                'Phone' => 'Phone',
                'Details' => 'Details',
                'Skills' => 'Skills',
                'Add up to 10 skills' => 'Add up to 10 skills',
                'Title' => 'Title',
                'Job Title' => 'Job Title',
                'Introduce Yourself' => 'Introduce Yourself',
                'Save Changes' => 'Save Changes'
            ]
        ],

        'password' => [
            'Update password' => 'Update password',
            'Dashboard' => 'Dashboard',
            'Settings' => 'Settings',

            'form' => [
                'Password & Security' => 'Password & Security',
                'Current Password' => 'Current Password',
                'New Password' => 'New Password',
                'Repeat New Password' => 'Repeat New Password',
                'Save Changes' => 'Save Changes'
            ]
        ],

        'candidates' => [
            'index' => [
                'Applications' => 'Applications',
                'Dashboard' => 'Dashboard',
                'My Job Applications' => 'My Job Applications',
                'Nothing found' => 'Nothing found ...'
            ],

            'edit' => [
                'Applications' => 'Applications',
                'Dashboard' => 'Dashboard',
                'Details' => 'Details',
                'optional' => 'optional',
                'Experience' => 'Experience',
                'years' => 'years',
                'Detials about your experience' => 'Detials about your experience',
                'Save Changes' => 'Save Changes',
                'Delete' => 'Delete'
            ]
        ],

        'bookmarks' => [
            'index' => [
                'Bookmarks' => 'Bookmarks',
                'Dashboard' => 'Dashboard',
                'Bookmarked Jobs' => 'Bookmarked Jobs',
                'Bookmarked Companies' => 'Bookmarked Companies',
                'Bookmarked Freelancers' => 'Bookmarked Freelancers'
            ]
        ],

        'jobs' => [
            'index' => [
                'Dashboard' => 'Dashboard',
                'Manage Jobs' => 'Manage Jobs',
                'My Job Listings' => 'My Job Listings',
                'Manage Candidates' => 'Manage Candidates',
                'Edit' => 'Edit',
                'Remove' => 'Remove'
            ],

            'edit' => [
                'Dashboard' => 'Dashboard',
                'Manage Jobs' => 'Manage Jobs',
                'Job Edit Form' => 'Job Edit Form',
                'Job Title' => 'Job Title',
                'Salary' => 'Salary <span>(optional)</span>',
                'Job Type' => 'Job Type',
                'Job Category' => 'Job Category',
                'Job Description' => 'Job Description',
                'Save' => 'Save'
            ],

            'create' => [
                'Dashboard' => 'Dashboard',
                'Post a Job' => 'Post a Job',
                'Job Submission Form' => 'Job Submission Form',
                'Job Title' => 'Job Title',
                'Salary' => 'Salary <span>(optional)</span>',
                'Job Type' => 'Job Type',
                'Job Category' => 'Job Category',
                'Job Description' => 'Job Description',
                'Save' => 'Save'
            ],
        ]
    ]
];
