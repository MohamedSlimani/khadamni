<?php

return [
    'welcome' => [
        'hero' => [
            'title' => 'Hire experts or be hired for any job, any time.',
            'text' => 'Thousands of small businesses use <strong class="color">:name</strong> to turn their ideas into reality.',
        ],
        'stats' => [
            'jobs' => 'Jobs Posted',
            'freelancers' => 'Freelancers',
            'companies' => 'Companies'
        ],
        'Latest Jobs' => 'Latest Jobs',
        'Browse All Jobs' => 'Browse All Jobs'
    ],

    'jobs' => [
        'index' => [
            'Search Results' => 'Search Results',
            'Show Filters' => 'Show Filters',
            'Hide Filters' => 'Hide Filters'
        ],

        'show' => [
            'About the Employer' => 'About the Employer',
            'Salary' => 'Salary',
            'Job Description' => 'Job Description',
            'Location' => 'Location',
            'Similar Jobs' => 'Similar Jobs',
            'Job Summary' => 'Job Summary',
            'Job Type' => 'Job Type',
            'Date Posted' => 'Date Posted',
            'Bookmark or Share' => 'Bookmark or Share',
            'Interesting? Share It!' => 'Interesting? <strong>Share It!</strong>',
            'Copied to clipboard!' => 'Copied to clipboard!'
        ]
    ],

    'companies' => [
        'index' => [
            'Browse Companies' => 'Browse Companies',
            'Home' => 'Home',
            'Companies' => 'Companies',
            'Nothing found' => 'Nothing found ...'
        ],

        'show' => [
            'About Company' => 'About Company',
            'Open Positions' => 'Open Positions',
            'Location' => 'Location',
            'Social Profiles' => 'Social Profiles',
            'Bookmark or Share' => 'Bookmark or Share',
            'Interesting? Share It!' => 'Interesting? <strong>Share It!</strong>',
            'Copied to clipboard!' => 'Copied to clipboard!'
        ]
    ],

    'users' => [
        'index' => [
            'Search Results' => 'Search Results',
            'Location' => 'Location',
            'Nothing found' => 'Nothing found ...',
            'Location not available' => 'Localisation non disponible',
            'View Profile' => 'View Profile'
        ],

        'show' => [
            'About Me' => 'About Me',
            'Skills' => 'Skills',
            'Attachments' => 'Attachments',
            'Social Profiles' => 'Social Profiles',
            'Bookmark or Share' => 'Bookmark or Share',
            'Interesting? Share It!' => 'Interesting? <strong>Share It!</strong>',
            'Copied to clipboard!' => 'Copied to clipboard!'
        ]
    ],

    'how-it-works' => [
        'How does it work' => 'How does it work?',
        'Home' => 'Home',
    ],

    'Contact' => [
        'Contact' => 'Contact',
        'Home' => 'Home',
        'Our Office' => 'Our Office',
        'Any questions' => 'Any questions? Feel free to contact us!',

        'form' => [
            'Your Name' => 'Your Name',
            'Email Address' => 'Email Address',
            'Subject' => 'Subject',
            'Message' => 'Message',
            'Submit Message' => 'Submit Message'
        ]
    ]
];
