<?php

return [
    'Log In' => 'Log In',
    'Home' => 'Home',
    'We\'re glad to see you again!' => 'We\'re glad to see you again!',
    'Don\'t have an account? ' => 'Don\'t have an account? ',
    'Sign Up!' => 'Sign Up!',

    'form' => [
        'Email' => 'Email',
        'Password' => 'Password',
        'Remember Me' => 'Remember Me',
        'Full name' => 'Full name',
        'Repeat Password' => 'Repeat Password'
    ],

    'Log In via Facebook' => 'Log In via Facebook',
    'Log In via Google' => 'Log In via Google',

    'Register via Facebook' => 'Register via Facebook',
    'Register via Google' => 'Register via Google',

    'Register' => 'Register',
    'Let\'s create your account!' => 'Let\'s create your account!',
    'Already have an account?' => 'Already have an account?',
    'Log In!' => 'Log In!',

    'or' => 'or'

];
