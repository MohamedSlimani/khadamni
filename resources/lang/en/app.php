<?php

return [

    'header' => [
        'Find Work' => 'Find Work',
        'Browse Jobs' => 'Browse Jobs',
        'Browse Companies' => 'Browse Companies',
        'For Employers' => 'For Employers',
        'Find a Freelancer' => 'Find a Freelancer',
        'Post a Job' => 'Post a Job',
        'How it works' => 'How it works',
        'Contact' => 'Contact',
        'Log In / Register' => 'Log In / Register',
    ],

    'footer' => [
        'For Candidates' => 'For Candidates',
        'Browse Jobs' => 'Browse Jobs',
        'Browse Companies' => 'Browse Companies',
        'Add Resume' => 'Add Resume',
        'My Bookmarks' => 'My Bookmarks',
        'For Employers' => 'For Employers',
        'Find a Freelancer' => 'Find a Freelancer',
        'Post a Job' => 'Post a Job',
        'Helpful Links' => 'Helpful Links',
        'How it works' => 'How it works',
        'Contact' => 'Contact',
        'Privacy Policy' => 'Privacy Policy',
        'Terms of Use' => 'Terms of Use',
        'Account' => 'Account',
        'My Account' => 'Mon compte',
        'Log In' => 'Log In',
        'Subscribe' => 'Sign Up For Our Newsletter',
        'Subscribe text' => 'Weekly breaking news, analysis and cutting edge advices on job searching.',
        'Subscribe placeholder' => 'Enter your email address'
    ],

    'Routes' => [
        'Dashboard' => 'Dashboard',
        'Settings' => 'Settings',
    ],

    'Employer' => 'Employer',
    'Freelancer' => 'Freelancer',

    'types' => [
        'Full Time' => 'Full Time',
        'Freelance' => 'Freelance',
        'Part Time' => 'Part Time',
        'Internship' => 'Internship',
        'Temporary' => 'Temporary'
    ],

    'categories' => [
        'Agriculture, Food and Natural Resources' => 'Agriculture, Food and Natural Resources',
        'Architecture and Construction' => 'Architecture and Construction',
        'Arts, Audio/Video Technology and Communications' => 'Arts, Audio/Video Technology and Communications',
        'Business Management and Administration' => 'Business Management and Administration',
        'Education and Training' => 'Education and Training',
        'Finance' => 'Finance',
        'Government and Public Administration' => 'Government and Public Administration',
        'Health Science' => 'Health Science',
        'Hospitality and Tourism' => 'Hospitality and Tourism',
        'Human Services' => 'Human Services',
        'Information Technology' => 'Information Technology',
        'Law, Public Safety, Corrections and Security' => 'Law, Public Safety, Corrections and Security',
        'Manufacturing' => 'Manufacturing',
        'Marketing, Sales and Service' => 'Marketing, Sales and Service',
        'Science, Technology, Engineering and Mathematics' => 'Science, Technology, Engineering and Mathematics',
        'Transportation, Distribution & Logistics' => 'Transportation, Distribution & Logistics',
    ],

];
