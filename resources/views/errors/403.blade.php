@extends('layouts.app')

@section('content')

    <!-- Titlebar
================================================== -->
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>403 Forbidden</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li>403 Forbidden</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <!-- Page Content
    ================================================== -->
    <!-- Container -->
    <div class="container">

        <div class="row">
            <div class="col-xl-12">

                <section id="not-found" class="center margin-top-50 margin-bottom-25">
                    <h2>403 <i class="icon-line-awesome-exclamation-circle"></i></h2>
                    <p>You are not allowed here</p>
                </section>

                <div class="row">
                    <div class="col-xl-8 offset-xl-2">
                        <div class="margin-bottom-50" style="display: flex; justify-content: center">
                            <a href="{{ route('welcome') }}" class="button ripple-effect">Home</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- Container / End -->
@endsection
