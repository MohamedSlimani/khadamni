@extends('layouts.app')

@section('content')

    <!-- Titlebar
================================================== -->
    <div class="single-page-header" data-background-image="{{ asset('images/single-job.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-page-header-inner">
                        <div class="left-side">
                            <div class="header-image"><a href="{{ route('companies.show',$job->company->id) }}">
                                    <img src="{{ $job->company->image() }}" alt=""></a></div>
                            <div class="header-details">
                                <h3>{{ $job->title }}</h3>
                                <h5>{{ __('front.jobs.show.About the Employer') }}</h5>
                                <ul>
                                    <li>
                                        <a href="{{ route('companies.show',$job->company->id) }}">
                                            <i class="icon-material-outline-business"></i>{{ $job->company->name }}</a>
                                    </li>
                                    {{--<li>
                                        <div class="star-rating" data-rating="4.9"></div>
                                    </li>--}}
                                    <li>{{ $job->company->location }}</li>
                                    {{--<li>
                                        <div class="verified-badge-with-title">Verified</div>
                                    </li>--}}
                                </ul>
                            </div>
                        </div>
                        @if($job->salary_min or $job->salary_man)
                            <div class="right-side">
                                <div class="salary-box">
                                    <div class="salary-type">{{ __('front.jobs.show.Salary') }}</div>
                                    @if($job->salary_min && $job->salary_man)
                                        <div class="salary-amount">{{ $job->salary_min }} - {{ $job->salary_max }}DZD
                                        </div>
                                    @else
                                        @if($job->salary_min)
                                            <div class="salary-amount">{{ $job->salary_min }} DZD</div>
                                        @else
                                            <div class="salary-amount">{{ $job->salary_mxn }} DZD</div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Page Content
    ================================================== -->
    <div class="container">
        <div class="row">

            <!-- Content -->
            <div class="col-xl-8 col-lg-8 content-right-offset">

                <div class="single-page-section">
                    <h3 class="margin-bottom-25">{{ __('front.jobs.show.Job Description') }}</h3>
                    {!! html_entity_decode($job->description) !!}
                </div>

                <div class="single-page-section">
                    <h3 class="margin-bottom-30">{{ __('front.jobs.show.Location') }}</h3>
                    <div id="single-job-map-container">
                        <div id="singleListingMap" data-latitude="{{ $job->lat }}" data-longitude="{{ $job->lng }}"
                             data-map-icon="im im-icon-Hamburger"></div>
                        <a href="#" id="streetView">Street View</a>
                    </div>
                </div>

                <div class="single-page-section">
                <!--
                    <h3 class="margin-bottom-25">{{ __('front.jobs.show.Similar Jobs') }}</h3>
-->
                    <!-- Listings Container -->
                    <div class="listings-container grid-layout">

                    @foreach($job->similarJobs() as $similarJob)
                        <!-- Job Listing -->
                            <a href="{{ route('jobs.show', $similarJob->id) }}" class="job-listing">

                                <!-- Job Listing Details -->
                                <div class="job-listing-details">
                                    <!-- Logo -->
                                    <div class="job-listing-company-logo">
                                        <img src="{{ $similarJob->company->image() }}" alt="">
                                    </div>

                                    <!-- Details -->
                                    <div class="job-listing-description">
                                        <h4 class="job-listing-company">{{ $similarJob->company->name }}</h4>
                                        <h3 class="job-listing-title">{{ $similarJob->title }}</h3>
                                    </div>
                                </div>

                                <!-- Job Listing Footer -->
                                <div class="job-listing-footer">
                                    <ul>
                                        <li><i class="icon-material-outline-location-on"></i>{{ $similarJob->location }}
                                        </li>
                                        <li><i class="icon-material-outline-business-center"></i>{{ $similarJob->type }}
                                        </li>
                                        <li>
                                            <i class="icon-material-outline-access-time"></i>{{ $similarJob->created_at->diffForHumans() }}
                                        </li>
                                    </ul>
                                </div>
                            </a>
                        @endforeach

                    </div>
                    <!-- Listings Container / End -->

                </div>
            </div>

            <!-- Sidebar -->
            <div class="col-xl-4 col-lg-4">
                <div class="sidebar-container">

                    @guest()

                        <a href="{{ route('register') }}?job={{ $job->id }}" class="apply-now-button">
                            Postuler <i class="icon-material-outline-arrow-right-alt"></i>
                        </a>

                    @else
                        @if($candidate)
                            <a href="{{ route('dashboard.candidates.edit', $candidate->id) }}"
                               class="apply-now-button " style="background-color: #38b653">
                                Visualiser candidature <i class="icon-material-outline-check-circle"></i>
                            </a>
                        @else
                            @if(!Auth::user()->employer)
                            <a href="{{ route('dashboard.candidates.store') }}" onclick="event.preventDefault();
                                                     document.getElementById('apply-form').submit();" class="apply-now-button">
                                Postuler <i class="icon-material-outline-arrow-right-alt"></i>
                            </a>
                            <form id="apply-form" action="{{ route('dashboard.candidates.store') }}" method="POST" style="display: none;">
                                @csrf
                                <input type="hidden" name="job" value="{{ $job->id }}">
                            </form>
                            @endif
                        @endif

                    @endguest

                    <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <div class="job-overview">
                            <div class="job-overview-headline">{{ __('front.jobs.show.Job Summary') }}</div>
                            <div class="job-overview-inner">
                                <ul>
                                    <li>
                                        <i class="icon-material-outline-location-on"></i>
                                        <span>{{ __('front.jobs.show.Location') }}</span>
                                        <h5>{{ $job->location }}</h5>
                                    </li>
                                    <li>
                                        <i class="icon-material-outline-business-center"></i>
                                        <span>{{ __('front.jobs.show.Job Type') }}</span>
                                        <h5>{{ $job->type }}</h5>
                                    </li>
                                    <li>
                                        <i class="icon-material-outline-local-atm"></i>
                                        <span>{{ __('front.jobs.show.Salary') }}</span>
                                        <h5>
                                            @if($job->salary_min or $job->salary_man)
                                                @if($job->salary_min && $job->salary_man)
                                                    {{ $job->salary_min }} - {{ $job->salary_max }}DZD
                                                @else
                                                    @if($job->salary_min)
                                                        {{ $job->salary_min }} DZD
                                                    @else
                                                        {{ $job->salary_mxn }} DZD
                                                    @endif
                                                @endif
                                            @endif
                                        </h5>
                                    </li>
                                    <li>
                                        <i class="icon-material-outline-access-time"></i>
                                        <span>{{ __('front.jobs.show.Date Posted') }}</span>
                                        <h5>{{ $job->created_at->diffForHumans() }}</h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <h3>{{ __('front.jobs.show.Bookmark or Share') }}</h3>

                        <!-- Bookmark Button -->
                        <book-mark
                            class="margin-bottom-25"
                            show-button
                            id="{{ $job->bookmark() ? $job->bookmark()->id : '' }}"
                            model-id="{{ $job->id }}"
                            model-type="job"
                            add-route="{{ route('dashboard.bookmarks.store') }}"
                            remove-route="{{ route('dashboard.bookmarks.destroy','_id_' ) }}"
                        ></book-mark>

                        <!-- Copy URL -->
                        <div class="copy-url">
                            <input id="copy-url" type="text" value="" class="with-border">
                            <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url"
                                    title="Copy to Clipboard" data-tippy-placement="top"><i
                                    class="icon-material-outline-file-copy"></i></button>
                        </div>

                        <!-- Share Buttons -->
                        <!--
                        <div class="share-buttons margin-top-25">
                            <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                            <div class="share-buttons-content">
                                <span>@lang('front.jobs.show.Interesting? Share It!')</span>
                                <ul class="share-buttons-icons">
                                    <li><a href="#" data-button-color="#3b5998" title="Share on Facebook"
                                           data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
                                    <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter"
                                           data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                    <li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus"
                                           data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
                                    <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn"
                                           data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        -->
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection

@section('after-js')

    <!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
    <script>

    // Snackbar for copy to clipboard button
    $('.copy-url-button').click(function () {
        Snackbar.show({
            text: '{{ __('front.jobs.show.Copied to clipboard!') }}',
        })
    })
    </script>

    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ config('google-maps.key') }}&libraries=places"></script>
    <script src="{{ asset('js/maps/infobox.min.js') }}"></script>
    <script src="{{ asset('js/markerclusterer.js') }}"></script>
    <script src="{{ asset('js/maps.js') }}"></script>
@endsection
