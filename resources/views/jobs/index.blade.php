@extends('layouts.app-no-footer')

@section('content')
    <!-- Page Content
================================================== -->
    <div class="full-page-container with-map">

    @include('jobs.partials.sidebar')
    <!-- Full Page Sidebar / End -->

        <!-- Full Page Content -->
        <div class="full-page-content-container" data-simplebar>
            <div class="full-page-content-inner">

                <h3 class="page-title">{{ __('front.jobs.index.Search Results') }}</h3>
                <!--
                <div class="notify-box margin-top-15">
                    <div class="switch-container">
                        <label class="switch"><input type="checkbox"><span class="switch-button"></span><span
                                class="switch-text">Turn on email alerts for this search</span></label>
                    </div>

                    <sort-by></sort-by>
                </div>
                -->
                <div class="listings-container compact-list-layout margin-top-35 margin-bottom-25">

                    <!-- Job Listing -->
                    @foreach($jobs as $job)
                        <a href="{{ route('jobs.show', $job->id) }}" class="job-listing">

                            <!-- Job Listing Details -->
                            <div class="job-listing-details">

                                <!-- Logo -->
                                <div class="job-listing-company-logo">
                                    <img src="{{ $job->company->image() }}" alt="">
                                </div>

                                <!-- Details -->
                                <div class="job-listing-description">
                                    <h3 class="job-listing-title">{{ $job->title }}</h3>

                                    <!-- Job Listing Footer -->
                                    <div class="job-listing-footer">
                                        <ul>
                                            <li><i class="icon-material-outline-business"></i>{{ $job->company->name }}
                                                <div class="verified-badge" title="Verified Employer"
                                                     data-tippy-placement="top"></div>
                                            </li>
                                            <li><i class="icon-material-outline-location-on"></i>{{ $job->location }}
                                            </li>
                                            <li><i class="icon-material-outline-description"></i>{{ $job->category }}
                                            <li><i class="icon-material-outline-business-center"></i>{{ $job->type }}
                                            </li>
                                            <li>
                                                <i class="icon-material-outline-access-time"></i>{{ $job->created_at->diffForHumans() }}
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <!-- Bookmark -->
                                @auth()
                                    <book-mark id="{{ $job->bookmark() ? $job->bookmark()->id : '' }}"
                                               model-id="{{ $job->id }}"
                                               model-type="job"
                                               add-route="{{ route('dashboard.bookmarks.store') }}"
                                               remove-route="{{ route('dashboard.bookmarks.destroy','_id_' ) }}"
                                    ></book-mark>
                                @endauth
                            </div>
                        </a>
                    @endforeach

                </div>

                <!-- Pagination -->
                <div class="clearfix"></div>

                {{ $jobs->links('layouts.pagination.dafault') }}

                <div class="clearfix"></div>
                <!-- Pagination / End -->

                <!-- Footer -->
                @include('layouts.partials.small-footer')
                <!-- Footer / End -->

            </div>
        </div>
        <!-- Full Page Content / End -->

        <!-- Full Page Map -->
        <div class="full-page-map-container">

            <!-- Enable Filters Button -->
            <div class="filter-button-container">
                <button class="enable-filters-button">
                    <i class="enable-filters-button-icon"></i>
                    <span class="show-text">{{ __('front.jobs.index.Show Filters') }}</span>
                    <span class="hide-text">{{ __('front.jobs.index.Hide Filters') }}</span>
                </button>
                {{--<div class="filter-button-tooltip">Click to expand sidebar with filters!</div>--}}
            </div>

            <!-- Map -->
            <jobs-map jobs="{{ $jobs->toJson() }}"></jobs-map>
        </div>
        <!-- Full Page Map / End -->

    </div>
@endsection

@section('before-js')
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ config('google-maps.key') }}&libraries=places"></script>
    <script src="{{ asset('js/maps/infobox.min.js') }}"></script>
    <script src="{{ asset('js/maps/markerclusterer.js') }}"></script>
    <script src="{{ asset('js/maps/custom-marker.js') }}"></script>
@endsection
