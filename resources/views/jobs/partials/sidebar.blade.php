<div class="full-page-sidebar hidden-sidebar">
    <div class="full-page-sidebar-inner" data-simplebar>

        <div class="sidebar-container">

            <!-- Location -->
            <locations class="sidebar-widget"
                       form-id="job-search-form"
                       
                       no-map
            ></locations>

            <!-- Keywords -->
            <tags-editor form-id="job-search-form" name="keywords" title="Keywords" placeholder="e.g. job title"></tags-editor>

            <!-- Category -->
            <search-switches title="Categories" name="categories" form-id="job-search-form" options="{{ json_encode($categories) }}"></search-switches>

            <!-- Job Types -->
            <search-switches title="Job Type" name="types" form-id="job-search-form" options="{{ json_encode($types) }}"></search-switches>


            <!-- Salary -->
            <div class="sidebar-widget">
                <h3>Salary</h3>
                <div class="margin-top-55"></div>

                <!-- Range Slider -->
                <input class="range-slider" type="text" name="salary" form="search" value="" data-slider-currency="DZD" data-slider-min="0"
                       data-slider-max="200000" data-slider-step="100" data-slider-value="[0,200000]" />
            </div>

            {{--<!-- Tags -->
            <div class="sidebar-widget">
                <h3>Tags</h3>

                <div class="tags-container">
                    <div class="tag">
                        <input type="checkbox" id="tag1" />
                        <label for="tag1">front-end dev</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag2" />
                        <label for="tag2">angular</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag3" />
                        <label for="tag3">react</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag4" />
                        <label for="tag4">vue js</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag5" />
                        <label for="tag5">web apps</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag6" />
                        <label for="tag6">design</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag7" />
                        <label for="tag7">wordpress</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag1" />
                        <label for="tag1">front-end dev</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag2" />
                        <label for="tag2">angular</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag3" />
                        <label for="tag3">react</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag4" />
                        <label for="tag4">vue js</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag5" />
                        <label for="tag5">web apps</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag6" />
                        <label for="tag6">design</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag7" />
                        <label for="tag7">wordpress</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag1" />
                        <label for="tag1">front-end dev</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag2" />
                        <label for="tag2">angular</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag3" />
                        <label for="tag3">react</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag4" />
                        <label for="tag4">vue js</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag5" />
                        <label for="tag5">web apps</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag6" />
                        <label for="tag6">design</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag7" />
                        <label for="tag7">wordpress</label>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>--}}

        </div>
        <!-- Sidebar Container / End -->

        <form id="job-search-form">
            <!-- Search Button -->
            <div class="sidebar-search-button-container">
                <button class="button ripple-effect">Search</button>
            </div>
            <!-- Search Button / End-->
        </form>

    </div>
</div>
