@extends('layouts.dashboard')

@section('content')
    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
        <h3>Gérer les candidatures</h3>
    <!--
        <span class="margin-top-7">Les candidatures de <a href="#">{{ $job->name }}</a></span> -->

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="{{ route('dashboard.home') }}">Dashboard</a></li>
                <li><a href="{{ route('dashboard.jobs.index') }}">offres d'emploi</a></li>
                <li>Gérer les candidatures</li>
            </ul>
        </nav>
    </div>

    <!-- Row -->
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">

                <!-- Headline -->
                <div class="headline">
                    <h3><i class="icon-material-outline-supervisor-account"></i> {{ $job->candidates->count() }}
                        Candidates</h3>
                </div>

                <div class="content">
                    <ul class="dashboard-box-list">
                        @foreach($job->candidates as $candidate)
                            <li>
                                <!-- Overview -->
                                <div class="freelancer-overview manage-candidates">
                                    <div class="freelancer-overview-inner">
                                        <book-mark
                                            id="{{ $candidate->user->freelancerBookmark() ? $candidate->user->freelancerBookmark()->id : '' }}"
                                            model-id="{{ $candidate->user->id }}"
                                            model-type="freelancer"
                                            add-route="{{ route('dashboard.bookmarks.store') }}"
                                            remove-route="{{ route('dashboard.bookmarks.destroy','_id_' ) }}"
                                        ></book-mark>

                                        <!-- Avatar -->
                                        <div class="freelancer-avatar">
                                            {{--<div class="verified-badge"></div>--}}
                                            <a href="{{ route('users.show',$candidate->user->id) }}"><img
                                                    src="{{ $candidate->user->image() }}" alt=""></a>
                                        </div>

                                        <!-- Name -->
                                        <div class="freelancer-name">
                                            <h4><a href="{{ route('users.show',$candidate->user->id) }}">
                                                {{ $candidate->user->name }}
                                            </h4>

                                            <!-- Details -->
                                            <span class="freelancer-detail-item">
                                                <i class="icon-line-awesome-map-marker"></i> {{ $candidate->user->location ?? 'localisation non disponible' }}
                                            </span>
                                            <span class="freelancer-detail-item">
                                                <a href="mailto://{{ $candidate->user->email }}">
                                                    <i class="icon-feather-mail"></i> {{ $candidate->user->email }}
                                                </a>
                                            </span>
                                            <span class="freelancer-detail-item">
                                                <i class="icon-feather-phone"></i> {{ $candidate->user->phone }}
                                            </span>

                                            <!-- Rating -->
                                        {{--<div class="freelancer-rating">
                                            <div class="star-rating" data-rating="5.0"></div>
                                        </div>--}}

                                        <!-- Buttons -->
                                            <div class="buttons-to-right always-visible margin-top-25 margin-bottom-5">
                                                @if(count($candidate->user->getMedia('attachments')))
                                                    <a href="{{ route('download', $candidate->user->getMedia('attachments')->first()->getAttribute('uuid')) }}"
                                                       class="button ripple-effect"><i
                                                            class="icon-feather-file-text"></i> Download CV</a>
                                                @endif
                                                <a href="{{ route('dashboard.jobs.candidates.show',[$job->id,  $candidate->id]) }}"
                                                class="button gray ripple-effect ico"
                                                   title="Show Candidate"
                                                   data-tippy-placement="top">
                                                    <i class="icon-feather-zoom-in"></i>
                                                </a>
                                                <a href="#" class="button gray ripple-effect ico"
                                                   title="Remove Candidate"
                                                   onclick="event.preventDefault();
                                                     document.getElementById('delete-form-{{ $candidate->id }}').submit();"
                                                   data-tippy-placement="top">
                                                    <i class="icon-feather-trash-2"></i>
                                                </a>
                                                <form id="delete-form-{{ $candidate->id }}"
                                                      action="{{ route('dashboard.jobs.candidates.destroy',[$job->id,  $candidate->id]) }}"
                                                      method="POST"
                                                      style="display: none;">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <!-- Row / End -->
@endsection
