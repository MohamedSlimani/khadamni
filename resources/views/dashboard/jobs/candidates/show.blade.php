@extends('layouts.dashboard')

@section('content')
    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
        <h3>Gérer les candidatures</h3>
    <!--
        <span class="margin-top-7">Les candidatures de <a href="#">{{ $job->name }}</a></span> -->

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="{{ route('dashboard.home') }}">Dashboard</a></li>
                <li><a href="{{ route('dashboard.jobs.index') }}">offres d'emploi</a></li>
                <li><a href="{{ route('dashboard.jobs.show', $job->id) }}">{{ $job->title }}</a></li>
                <li><a href="{{ route('dashboard.jobs.candidates', $job->id ) }}">Gérer les candidatures</a></li>
                <li>{{ $candidate->user->name }}</li>
            </ul>
        </nav>
    </div>

    <!-- Row -->
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">

                <!-- Headline -->
                <div class="headline">
                    <div class="left-side">
                        <div class="header-image freelancer-avatar"><img src="{{ $user->image() }}" alt=""></div>
                        <div class="header-details">
                            <h3>{{ $user->name }} <span>{{ $user->title }}</span></h3>
                            <ul>
                                {{--<li><div class="star-rating" data-rating="5.0"></div></li>--}}
                                <li><i class="icon-material-outline-location-on"></i> {{ $user->location }}</li>
                                <li><a href="mailto://{{ $user->email }}"><i class="icon-material-outline-email"></i> {{ $user->email }}</a></li>
                                @auth
                                <li><a href="tel://{{ $user->phone }}"><i class="icon-feather-phone-call"></i> {{ $user->phone }}</a></li>
                                @endauth
                                {{--<li><div class="verified-badge-with-title">Verified</div></li>--}}
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="content padding-left-25 padding-top-25 padding-right-25 padding-bottom-25">
                    <div class="row">

                        <!-- Content -->
                        <div class="col-xl-8 col-lg-8 content-right-offset">

                            <!-- Page Content -->
                            <div class="single-page-section">
                                <h3 class="margin-bottom-25">{{ __('front.users.show.About Me') }}</h3>
                                {!! html_entity_decode($user->description) !!}
                            </div>

                            <div class="single-page-section">
                                <h3 class="margin-bottom-25">{{ __('front.users.show.Candidation Description') }}</h3>
                                <h3 class="margin-bottom-25">Experience {{ $candidate->experience }}</h3>
                                {!! html_entity_decode($candidate->description) !!}
                            </div>



                        </div>

                        <!-- Sidebar -->
                        <div class="col-xl-4 col-lg-4">
                            <div class="sidebar-container">




                            <!-- Widget -->
                                <div class="sidebar-widget">
                                    <h3>{{ __('front.users.show.Skills') }}</h3>
                                    <div class="task-tags">
                                        @if($user->skills)
                                        @foreach($user->skills as $skill)
                                            <span>{{ $skill }}</span>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>

                                <!-- Widget -->
                                @guest
                                @else
                                <div class="sidebar-widget">
                                    <h3>{{ __('front.users.show.Attachments') }}</h3>
                                    <div class="attachments-container">
                                        @foreach($user->getMedia('attachments') as $attachment)
                                            <a href="{{ route('download', $attachment->getAttribute('uuid')) }}"
                                               class="attachment-box ripple-effect">
                                                <span>{{ $attachment->getAttribute('name') }}</span>
                                                <i>{{ strtoupper(explode('/', $attachment->getAttribute('mime_type'))[1]) }}</i>
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                                @endguest

                                <!-- Sidebar Widget -->
                                <div class="sidebar-widget">
                                    <h3>{{ __('front.users.show.Bookmark or Share') }}</h3>

                                    <!-- Bookmark Button -->
                                    <book-mark
                                        class="margin-bottom-25"
                                        show-button
                                        id="{{ $user->freelancerBookmark() ? $user->freelancerBookmark()->id : '' }}"
                                        model-id="{{ $user->id }}"
                                        model-type="freelancer"
                                        add-route="{{ route('dashboard.bookmarks.store') }}"
                                        remove-route="{{ route('dashboard.bookmarks.destroy','_id_' ) }}"
                                    ></book-mark>

                                    <!-- Copy URL -->
                                    <div class="copy-url">
                                        <input id="copy-url" type="text" value="" class="with-border">
                                        <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url"
                                                title="Copy to Clipboard" data-tippy-placement="top"><i
                                                class="icon-material-outline-file-copy"></i></button>
                                    </div>

                                    <!-- Share Buttons -->
                                    <div class="share-buttons margin-top-25">
                                        <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                                        <div class="share-buttons-content">
                                            <span>@lang('front.companies.show.Interesting? Share It!')</span>
                                            <ul class="share-buttons-icons">
                                                <li><a href="#" data-button-color="#3b5998" title="Share on Facebook"
                                                       data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
                                                <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter"
                                                       data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                                <li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus"
                                                       data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
                                                <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn"
                                                       data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Row / End -->
@endsection
