@extends('layouts.dashboard')

@section('content')
    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
        <h3>{{ __('dashboard.settings.jobs.create.Post a Job') }}</h3>
        {{ env('APP_NAME') }}

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="{{ route('dashboard.home') }}">{{ __('dashboard.settings.jobs.create.Dashboard') }}</a></li>
                <li>{{ __('dashboard.settings.jobs.create.Post a Job') }}</li>
            </ul>
        </nav>
    </div>

    @if ($errors->any())
        <div class="notification error closeable">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <a class="close"></a>
        </div>
    @endif

    <!-- Row -->
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">

                <!-- Headline -->
                <div class="headline">
                    <h3><i class="icon-feather-folder-plus"></i> {{ __('dashboard.settings.jobs.create.Job Submission Form') }}</h3>
                </div>

                <div class="content with-padding padding-bottom-10">
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="submit-field">
                                <h5>{{ __('dashboard.settings.jobs.create.Job Title') }}</h5>
                                <input form="new-job" type="text" name="title" class="with-border"
                                       placeholder="{{ __('dashboard.settings.jobs.create.Job Title') }}">
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <div class="submit-field">
                                <h5>@lang('dashboard.settings.jobs.create.Salary')</h5>
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="input-with-icon">
                                            <input form="new-job" class="with-border" name="salary_min" type="text" placeholder="Min">
                                            <i class="currency">DZD</i>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="input-with-icon">
                                            <input form="new-job" class="with-border" name="salary_max" type="text" placeholder="Max">
                                            <i class="currency">DZD</i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <div class="submit-field">
                                <h5>{{ __('dashboard.settings.jobs.create.Job Type') }}</h5>
                                <select name="type" class="selectpicker with-border" data-size="7"
                                        form="new-job"
                                        title="Type du poste">
                                    <option value="Full Time">{{ __('app.types.Full Time') }}</option>
                                    <option value="Freelance">{{ __('app.types.Freelance') }}</option>
                                    <option value="Part Time">{{ __('app.types.Part Time') }}</option>
                                    <option value="Internship">{{ __('app.types.Internship') }}</option>
                                    <option value="Temporary">{{ __('app.types.Temporary') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <div class="submit-field">
                                <h5>{{ __('dashboard.settings.jobs.edit.Job Category') }}</h5>
                                <select name="category" class="selectpicker with-border" data-size="7"
                                        form="new-job"
                                        title="sélectionner le domaine">
                                    <option value="Agriculture, alimentation et ressources naturelles">
                                        {{ __('app.categories.Agriculture, Food and Natural Resources') }}
                                    </option>
                                    <option value="Architecture et construction">
                                        {{ __('app.categories.Architecture and Construction') }}
                                    </option>
                                    <option value="Arts, technologie audio / vidéo et communications">
                                        {{ __('app.categories.Arts, Audio/Video Technology and Communications') }}
                                    </option>
                                    <option value="Gestion et administration des affaires">
                                        {{ __('app.categories.Business Management and Administration') }}
                                    </option>
                                    <option value="Éducation et formation">
                                        {{ __('app.categories.Education and Training') }}
                                    </option>
                                    <option value="La finance">
                                        {{ __('app.categories.Finance') }}
                                    </option>
                                    <option value="Gouvernement et administration publique">
                                        {{ __('app.categories.Government and Public Administration') }}
                                    </option>
                                    <option value="Sciences de la santé">
                                        {{ __('app.categories.Health Science') }}
                                    </option>
                                    <option value="Hospitalité et Tourisme">
                                        {{ __('app.categories.Hospitality and Tourism') }}
                                    </option>
                                    <option value="Services humains">
                                        {{ __('app.categories.Human Services') }}
                                    </option>
                                    <option value="Informatique">
                                        {{ __('app.categories.Information Technology') }}
                                    </option>
                                    <option value="Droit, sécurité publique, correction et sécurité">
                                        {{ __('app.categories.Law, Public Safety, Corrections and Security') }}
                                    </option>
                                    <option value="Fabrication">
                                        {{ __('app.categories.Manufacturing') }}
                                    </option>
                                    <option value="Marketing, ventes et service">
                                        {{ __('app.categories.Marketing, Sales and Service') }}
                                    </option>
                                    <option value="Science, technologie, ingénierie et mathématiques">
                                        {{ __('app.categories.Science, Technology, Engineering and Mathematics') }}
                                    </option>
                                    <option value="Transport, distribution et logistique">
                                        {{ __('app.categories.Transportation, Distribution & Logistics') }}
                                    </option>
                                </select>
                            </div>
                        </div>

                        <tags-editor class="col-xl-12" form-id="new-job" title="Compétences" optional info="Maximum of 10 tags"></tags-editor>

                        <text-editor class="col-xl-12" form-id="new-job" name="description"
                                     title="{{ __('dashboard.settings.jobs.create.Job Description') }}"></text-editor>

                        <div class="col-xl-12">
                            <attachments
                                optional
                                info="You need to save First"
                                upload-route="{{ route('dashboard.jobs.attachments.temp') }}"
                                remove-route="{{ route('dashboard.attachments.remove', '_uuid_') }}"
                                :attachments="{{ Auth::user()->getMedia('tempJobsAttachments')->toJson() }}"></attachments>
                        </div>

                        <locations
                            class="col-xl-12"
                            form-id="new-job"
                            location="{{ Auth::user()->location ?? '' }}"
                            :lat="{{ Auth::user()->lat ?? '36' }}"
                            :lng="{{ Auth::user()->lng ?? '2' }}"
                            :map_zoom="{{ Auth::user()->map_zoom ?? '6' }}"></locations>
                    </div>
                </div>
            </div>
        </div>
        <form method="POST" action="{{ route('dashboard.jobs.store') }}" id="new-job">
            @csrf

            <div class="col-xl-12">
                <button type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i>
                    {{ __('dashboard.settings.jobs.create.Save') }}
                </button>
            </div>
        </form>

    </div>
    <!-- Row / End -->
@endsection

@section('before-js')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('google-maps.key') }}&libraries=places"></script>
@endsection
