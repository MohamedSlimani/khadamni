@extends('layouts.dashboard')

@section('content')
    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
        <h3>{{ $job->title }}</h3>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="{{ route('dashboard.home') }}">{{ __('dashboard.settings.jobs.edit.Dashboard') }}</a></li>
                <li><a href="{{ route('dashboard.jobs.index') }}">{{ __('dashboard.settings.jobs.edit.Manage Jobs') }}</a></li>
                <li>{{ $job->title }}</li>
            </ul>
        </nav>
    </div>

    @if ($errors->any())
        <div class="notification error closeable">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <a class="close"></a>
        </div>
    @endif

    <!-- Row -->
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">

                <!-- Headline -->
                <div class="headline">
                    <h3><i class="icon-feather-folder-plus"></i> {{ __('dashboard.settings.jobs.edit.Job Edit Form') }}</h3>
                </div>

                <div class="content with-padding padding-bottom-10">
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="submit-field">
                                <h5>{{ __('dashboard.settings.jobs.edit.Job Title') }}</h5>
                                <input form="update-job" type="text" name="title" class="with-border"
                                       value="{{ $job->title }}"
                                       placeholder="{{ __('dashboard.settings.jobs.edit.Job Title') }}">
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <div class="submit-field">
                                <h5>@lang('dashboard.settings.jobs.edit.Salary')</h5>
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="input-with-icon">
                                            <input form="update-job" class="with-border" name="salary_min"
                                                   value="{{ $job->salary_min }}"
                                                   type="text" placeholder="Min">
                                            <i class="currency">DZD</i>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="input-with-icon">
                                            <input form="update-job" class="with-border" name="salary_max"
                                                   value="{{ $job->salary_max }}" type="text" placeholder="Max">
                                            <i class="currency">DZD</i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <div class="submit-field">
                                <h5>{{ __('dashboard.settings.jobs.edit.Job Type') }}</h5>
                                <select name="type" class="selectpicker with-border" data-size="7"
                                        form="update-job"
                                        title="Select Job Type">
                                    <option {{ $job->type == 'Full Time' ? 'selected' : '' }}>{{ __('app.types.Full Time') }}</option>
                                    <option {{ $job->type == 'Freelance' ? 'selected' : '' }}>{{ __('app.types.Freelance') }}</option>
                                    <option {{ $job->type == 'Part Time' ? 'selected' : '' }}>{{ __('app.types.Part Time') }}</option>
                                    <option {{ $job->type == 'Internship' ? 'selected' : '' }}>{{ __('app.types.Internship') }}</option>
                                    <option {{ $job->type == 'Temporary' ? 'selected' : '' }}>{{ __('app.types.Temporary') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <div class="submit-field">
                                <h5>{{ __('dashboard.settings.jobs.edit.Job Category') }}</h5>
                                <select name="category" class="selectpicker with-border" data-size="7"
                                        form="update-job"
                                        title="Select Category">
                                    <option {{ $job->category == 'Agriculture, alimentation et ressources naturelles' ? 'selected' : '' }}
                                        value="Agriculture, alimentation et ressources naturelles">
                                        {{ __('app.categories.Agriculture, Food and Natural Resources') }}
                                    </option>
                                    <option {{ $job->category == 'Architecture et construction' ? 'selected' : '' }} value="Architecture et construction">
                                        {{ __('app.categories.Architecture and Construction') }}
                                    </option>
                                    <option {{ $job->category == 'Arts, technologie audio / vidéo et communications' ? 'selected' : '' }} value="Arts, technologie audio / vidéo et communications">
                                        {{ __('app.categories.Arts, Audio/Video Technology and Communications') }}
                                    </option>
                                    <option {{ $job->category == 'Gestion et administration des affaires' ? 'selected' : '' }} value="Gestion et administration des affaires">
                                        {{ __('app.categories.Business Management and Administration') }}
                                    </option>
                                    <option {{ $job->category == 'Éducation et formation' ? 'selected' : '' }} value="Éducation et formation">
                                        {{ __('app.categories.Education and Training') }}
                                    </option>
                                    <option {{ $job->category == 'La finance' ? 'selected' : '' }} value="La finance">
                                        {{ __('app.categories.Finance') }}
                                    </option>
                                    <option {{ $job->category == 'Gouvernement et administration publique' ? 'selected' : '' }}
                                        value="Gouvernement et administration publique">
                                        {{ __('app.categories.Government and Public Administration') }}
                                    </option>
                                    <option {{ $job->category == 'Sciences de la santé' ? 'selected' : '' }} value="Sciences de la santé">
                                        {{ __('app.categories.Health Science') }}
                                    </option>
                                    <option {{ $job->category == 'Hospitalité et Tourisme' ? 'selected' : '' }} value="Hospitalité et Tourisme">
                                        {{ __('app.categories.Hospitality and Tourism') }}
                                    </option>
                                    <option {{ $job->category == 'Services humains' ? 'selected' : '' }} value="Services humains">
                                        {{ __('app.categories.Human Services') }}
                                    </option>
                                    <option {{ $job->category == 'Informatique' ? 'selected' : '' }} value="Informatique">
                                        {{ __('app.categories.Information Technology') }}
                                    </option>
                                    <option {{ $job->category == 'Droit, sécurité publique, correction et sécurité' ? 'selected' : '' }}  value="Droit, sécurité publique, correction et sécurité">
                                        {{ __('app.categories.Law, Public Safety, Corrections and Security') }}
                                    </option>
                                    <option {{ $job->category == 'Fabrication' ? 'selected' : '' }} value="Fabrication">
                                        {{ __('app.categories.Manufacturing') }}
                                    </option>
                                    <option {{ $job->category == 'Marketing, ventes et service' ? 'selected' : '' }}  value="Marketing, ventes et service">
                                        {{ __('app.categories.Marketing, Sales and Service') }}
                                    </option>
                                    <option {{ $job->category == 'Science, technologie, ingénierie et mathématiques' ? 'selected' : '' }} value="Science, technologie, ingénierie et mathématiques">
                                        {{ __('app.categories.Science, Technology, Engineering and Mathematics') }}
                                    </option>
                                    <option {{ $job->category == 'Transport, distribution et logistique' ? 'selected' : '' }} value="Transport, distribution et logistique">
                                        {{ __('app.categories.Transportation, Distribution & Logistics') }}
                                    </option>
                                </select>
                            </div>
                        </div>

                        <tags-editor class="col-xl-12" form-id="update-job" title="Tags" optional
                                     info="Maximum of 10 tags" tags="{{ json_encode($job->tags) }}"></tags-editor>

                        <text-editor class="col-xl-12" form-id="update-job" name="description"
                                     content="{{ $job->description }}"
                                     title="{{ __('dashboard.settings.jobs.edit.Job Description') }}"></text-editor>

                        <div class="col-xl-12">
                            <attachments
                                optional
                                upload-route="{{ route('dashboard.jobs.attachments.add', $job->id) }}"
                                remove-route="{{ route('dashboard.jobs.attachments.delete', '_uuid_') }}"
                                :attachments="{{ $job->getMedia('attachments')->toJson() }}"></attachments>
                        </div>

                        <locations
                            class="col-xl-12"
                            form-id="update-job"
                            location="{{ $job->location }}"
                            :lat="{{ $job->lat }}"
                            :lng="{{ $job->lng }}"
                            :map_zoom="{{ $job->map_zoom }}"></locations>
                    </div>
                </div>
            </div>
        </div>
        <form method="POST" action="{{ route('dashboard.jobs.update', $job->id) }}" id="update-job">
            @method('PUT')
            @csrf

            <div class="col-xl-12">
                <button type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i>
                    {{ __('dashboard.settings.jobs.edit.Save') }}
                </button>
            </div>
        </form>

    </div>
    <!-- Row / End -->
@endsection

@section('before-js')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('google-maps.key') }}&libraries=places"></script>
@endsection
