@extends('layouts.dashboard')

@section('content')
    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
        <h3>{{ __('dashboard.sidebar.Manage Jobs') }}</h3>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="{{ route('dashboard.home') }}">{{ __('dashboard.settings.jobs.index.Dashboard') }}</a></li>
                <li>{{ __('dashboard.settings.jobs.index.Manage Jobs') }}</li>
            </ul>
        </nav>
    </div>

    <!-- Row -->
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">

                <!-- Headline -->
                <div class="headline">
                    <h3>
                        <i class="icon-material-outline-business-center"></i> {{ __('dashboard.settings.jobs.index.My Job Listings') }}
                    </h3>
                </div>

                <div class="content">
                    <ul class="dashboard-box-list">
                        @foreach(Auth::user()->jobs as $job)
                            <li>
                                <!-- Job Listing -->
                                <div class="job-listing">
                                    <!-- Job Listing Details -->
                                    <div class="job-listing-details">
                                        <!-- Details -->
                                        <div class="job-listing-description">
                                            <h3 class="job-listing-title">
                                                <a href="#">{{ $job->title }}</a>
                                                {{--<span class="dashboard-status-button green">Pending Approval</span>--}}
                                            </h3>

                                            <!-- Job Listing Footer -->
                                            <div class="job-listing-footer">
                                                <ul>
                                                    <li>
                                                        <i class="icon-material-outline-assignment"></i>
                                                        {{ $job->type }}
                                                    </li>
                                                    <li>
                                                        <i class="icon-material-outline-business-center"></i>
                                                        {{ $job->category }}
                                                    </li>
                                                    <li>
                                                        <i class="icon-material-outline-date-range"></i>
                                                        {{ $job->created_at->format('Y/m/d') }}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Buttons -->
                                <div class="buttons-to-right always-visible">
                                    <a href="{{ route('dashboard.jobs.candidates', $job->id) }}"
                                       class="button ripple-effect"><i
                                            class="icon-material-outline-supervisor-account"></i> {{ __('dashboard.settings.jobs.index.Manage Candidates') }}
                                        <span
                                            class="button-info">{{ $job->candidates->count() }}</span></a>
                                    <a href="{{ route('dashboard.jobs.edit', $job->id) }}"
                                       class="button gray ripple-effect ico"
                                       title="{{ __('dashboard.settings.jobs.index.Edit') }}"
                                       data-tippy-placement="top"><i class="icon-feather-edit"></i></a>
                                    <a href="#" class="button gray ripple-effect ico"
                                       onclick="event.preventDefault();
                                           document.getElementById('delete-form-{{ $job->id }}').submit();"
                                       title="{{ __('dashboard.settings.jobs.index.Remove') }}"
                                       data-tippy-placement="top">
                                        <i class="icon-feather-trash-2"></i></a>
                                    <form id="delete-form-{{ $job->id }}"
                                          action="{{ route('dashboard.jobs.destroy',$job->id) }}"
                                          method="POST"
                                          style="display: none;">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <!-- Row / End -->
@endsection
