@extends('layouts.dashboard')
@section('content')
    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
        <h3>Howdy, Tom!</h3>
        <span>We are glad to see you again!</span>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="#">Home</a></li>
                <li>Dashboard</li>
            </ul>
        </nav>
    </div>

    <!-- Fun Facts Container -->
    <div class="fun-facts-container">
        <div class="fun-fact" data-fun-fact-color="#36bd78">
            <div class="fun-fact-text">
                <span>Task Bids Won</span>
                <h4>22</h4>
            </div>
            <div class="fun-fact-icon"><i class="icon-material-outline-gavel"></i></div>
        </div>
        <div class="fun-fact" data-fun-fact-color="#b81b7f">
            <div class="fun-fact-text">
                <span>Jobs Applied</span>
                <h4>4</h4>
            </div>
            <div class="fun-fact-icon"><i class="icon-material-outline-business-center"></i></div>
        </div>
        <div class="fun-fact" data-fun-fact-color="#efa80f">
            <div class="fun-fact-text">
                <span>Reviews</span>
                <h4>28</h4>
            </div>
            <div class="fun-fact-icon"><i class="icon-material-outline-rate-review"></i></div>
        </div>

        <!-- Last one has to be hidden below 1600px, sorry :( -->
        <div class="fun-fact" data-fun-fact-color="#2a41e6">
            <div class="fun-fact-text">
                <span>This Month Views</span>
                <h4>987</h4>
            </div>
            <div class="fun-fact-icon"><i class="icon-feather-trending-up"></i></div>
        </div>
    </div>

    <!-- Row -->
    <div class="row">

        <div class="col-xl-6">
            <div class="dashboard-box main-box-in-row">
                <div class="headline">
                    <h3><i class="icon-feather-bar-chart-2"></i> Your Profile Views</h3>
                    <div class="sort-by">
                        <select class="selectpicker hide-tick">
                            <option>Last 6 Months</option>
                            <option>This Year</option>
                            <option>This Month</option>
                        </select>
                    </div>
                </div>
                <div class="content">
                    <!-- Chart -->
                    <div class="chart">
                        <canvas id="chart" width="100" height="45"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-6">
            <div class="dashboard-box">
                <div class="headline">
                    <h3><i class="icon-material-baseline-notifications-none"></i> Notifications</h3>
                    <button class="mark-as-read ripple-effect-dark" data-tippy-placement="left"
                            title="Mark all as read">
                        <i class="icon-feather-check-square"></i>
                    </button>
                </div>
                <div class="content">
                    <ul class="dashboard-box-list">
                        <li>
                                        <span class="notification-icon"><i
                                                class="icon-material-outline-group"></i></span>
                            <span class="notification-text">
										<strong>Michael Shannah</strong> applied for a job <a href="#">Full Stack Software Engineer</a>
									</span>
                            <!-- Buttons -->
                            <div class="buttons-to-right">
                                <a href="#" class="button ripple-effect ico" title="Mark as read"
                                   data-tippy-placement="left"><i class="icon-feather-check-square"></i></a>
                            </div>
                        </li>
                        <li>
                                        <span class="notification-icon"><i
                                                class=" icon-material-outline-gavel"></i></span>
                            <span class="notification-text">
										<strong>Gilber Allanis</strong> placed a bid on your <a href="#">iOS App Development</a> project
									</span>
                            <!-- Buttons -->
                            <div class="buttons-to-right">
                                <a href="#" class="button ripple-effect ico" title="Mark as read"
                                   data-tippy-placement="left"><i class="icon-feather-check-square"></i></a>
                            </div>
                        </li>
                        <li>
                            <span class="notification-icon"><i class="icon-material-outline-autorenew"></i></span>
                            <span class="notification-text">
										Your job listing <a href="#">Full Stack Software Engineer</a> is expiring
									</span>
                            <!-- Buttons -->
                            <div class="buttons-to-right">
                                <a href="#" class="button ripple-effect ico" title="Mark as read"
                                   data-tippy-placement="left"><i class="icon-feather-check-square"></i></a>
                            </div>
                        </li>
                        <li>
                                        <span class="notification-icon"><i
                                                class="icon-material-outline-group"></i></span>
                            <span class="notification-text">
										<strong>Sindy Forrest</strong> applied for a job <a href="#">Full Stack Software Engineer</a>
									</span>
                            <!-- Buttons -->
                            <div class="buttons-to-right">
                                <a href="#" class="button ripple-effect ico" title="Mark as read"
                                   data-tippy-placement="left"><i class="icon-feather-check-square"></i></a>
                            </div>
                        </li>
                        <li>
                                        <span class="notification-icon"><i
                                                class="icon-material-outline-rate-review"></i></span>
                            <span class="notification-text">
										<strong>David Peterson</strong> left you a <span class="star-rating no-stars"
                                                                                         data-rating="5.0"></span> rating after finishing <a
                                    href="#">Logo Design</a> task
									</span>
                            <!-- Buttons -->
                            <div class="buttons-to-right">
                                <a href="#" class="button ripple-effect ico" title="Mark as read"
                                   data-tippy-placement="left"><i class="icon-feather-check-square"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Row / End -->

@endsection
