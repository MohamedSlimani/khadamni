@extends('layouts.dashboard')

@section('content')
    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
        <h3>{{ __('dashboard.settings.account.Update Account') }}</h3>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="{{ route('dashboard.home') }}">{{ __('dashboard.settings.account.Dashboard') }}</a></li>
                <li>{{ __('dashboard.settings.account.Settings') }}</li>
                <li>{{ __('dashboard.settings.account.Update Account') }}</li>
            </ul>
        </nav>
    </div>

    <!-- Row -->

    @if ($errors->any())
        <div class="notification error closeable">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <a class="close"></a>
        </div>
    @endif

    <div class="row">

        <!-- Dashboard Box -->

        <div class="col-xl-12">
            <form id="settings-update" action="{{ route('dashboard.settings.update') }}" method="post"
                  enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="dashboard-box margin-top-0">

                    <!-- Headline -->
                    <div class="headline">
                        <h3><i class="icon-material-outline-account-circle"></i> {{ __('dashboard.settings.account.form.My Account') }}</h3>
                    </div>

                    <div class="content with-padding padding-bottom-0">

                        <div class="row">

                            <div class="col-sm-auto d-flex align-items-center justify-content-center">
                                <div class="avatar-wrapper" data-tippy-placement="bottom" title="{{ __('dashboard.settings.account.form.Change Avatar') }}">
                                    <img class="profile-pic"
                                         src="{{ Auth::user()->image() ?? asset('images/user-avatar-placeholder.png') }}"
                                         alt="" />
                                    <div class="upload-button"></div>
                                    <input class="file-upload" name="image" type="file" accept="image/*" />
                                </div>
                            </div>

                            <div class="col">
                                <div class="row">

                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>{{ __('dashboard.settings.account.form.Full Name') }}</h5>
                                            <input type="text" name="name" class="with-border" required
                                                   placeholder="{{ __('dashboard.settings.account.form.Full Name') }}"
                                                   value="{{ Auth::user()->name }}">
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>{{ __('dashboard.settings.account.form.Email') }}</h5>
                                            <input type="text" name="email" class="with-border" required
                                                   placeholder="{{ __('dashboard.settings.account.form.Email') }}"
                                                   value="{{ Auth::user()->email }}">
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <!-- Account Type -->
                                        <div class="submit-field">
                                            <h5>{{ __('dashboard.settings.account.form.Account Type') }}</h5>
                                            @if(Auth::user()->employer)
                                                <div class="account-type">
                                                    <div class="d-flex">
                                                        <button disabled
                                                                class="button ripple-effect green full-width margin-right-15">
                                                            <i class="icon-material-outline-business-center"></i>
                                                            {{ __('dashboard.settings.account.form.Employer') }}
                                                        </button>
                                                        <a href="{{ route('companies.show', Auth::id()) }}"
                                                           target="_blank"
                                                           class="button ripple-effect full-width">
                                                            {{ __('dashboard.settings.account.form.Preview') }} <i class="icon-material-outline-launch"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="account-type">
                                                    <div class="d-flex">
                                                        <button disabled
                                                                class="button ripple-effect green full-width margin-right-15">
                                                            <i class="icon-material-outline-account-circle"></i>
                                                            {{ __('dashboard.settings.account.form.Freelancer') }}
                                                        </button>
                                                        <a href="{{ route('companies.show', Auth::id()) }}"
                                                           target="_blank"
                                                           class="button ripple-effect full-width">
                                                            {{ __('dashboard.settings.account.form.Preview') }} <i class="icon-material-outline-launch"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>{{ __('dashboard.settings.account.form.Phone') }}</h5>
                                            <input type="tel" name="phone" class="with-border"
                                                   placeholder="+213 ..."
                                                   value="{{ Auth::user()->phone }}">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box">

                <!-- Headline -->
                <div class="headline">
                    <h3><i class="icon-material-outline-description"></i> {{ __('dashboard.settings.account.form.Details') }}</h3>
                </div>

                <div class="content">
                    <ul class="fields-ul">
                        @if(Auth::user()->freelancer)
                            <li>
                                <div class="row">
                                    <tags-editor
                                        class="col-xl-6"
                                        title="{{ __('dashboard.settings.account.form.Skills') }}"
                                        info="{{ __('dashboard.settings.account.form.Add up to 10 skills') }}"
                                        form-id="settings-update"
                                        tags="{{ Auth::user()->skills ? json_encode(Auth::user()->skills) : '' }}"
                                        name="skills"
                                    ></tags-editor>

                                    <div class="col-xl-6">
                                        <attachments
                                            upload-route="{{ route('dashboard.settings.upload') }}"
                                            remove-route="{{ route('dashboard.attachments.remove', '_uuid_') }}"
                                            :attachments="{{ Auth::user()->getMedia('attachments')->toJson() }}"
                                        ></attachments>
                                    </div>
                                </div>
                            </li>
                        @else
                            <input type="hidden" name="skills" form="settings-update" />
                        @endif
                        <li>
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>{{ __('dashboard.settings.account.form.Title') }}</h5>
                                        <input type="text" name="title" form="settings-update"
                                               value="{{ Auth::user()->title }}"
                                               class="with-border" placeholder="{{ __('dashboard.settings.account.form.Job Title') }}">
                                    </div>
                                </div>

                                <text-editor class="col-xl-12"
                                             name="description"
                                             title="{{ __('dashboard.settings.account.form.Introduce Yourself') }}"
                                             form-id="settings-update"
                                             content="{{ Auth::user()->description }}"></text-editor>

                                <locations class="col-xl-12"
                                           form-id="settings-update"
                                           :lat="{{ Auth::user()->lat ?? 34.8471273 }}"
                                           :lng="{{ Auth::user()->lng ?? 1.5450766 }}"
                                           :map_zoom="{{ Auth::user()->map_zoom ?? 6 }}"
                                           location="{{ Auth::user()->location ?? '' }}"
                                ></locations>

                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Button -->
        <div class="col-xl-12">
            <input type="hidden" form="settings-update" name="apply_to_job" id="applyToJob">
            <button type="submit" form="settings-update" class="button ripple-effect big margin-top-30">
                {{ __('dashboard.settings.account.form.Save Changes') }}
            </button>
        </div>

    </div>
    <!-- Row / End -->
@endsection

@section('before-js')
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ config('google-maps.key') }}&libraries=places"></script>
@endsection

@section('after-js')
    <script>
    jobid = localStorage.getItem('applyToJob')
    if (jobid) {
        $('#applyToJob').val(jobid)
    }
    </script>
@endsection
