@extends('layouts.dashboard')

@section('content')
    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
        <h3>{{ __('dashboard.settings.password.Update password') }}</h3>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="{{ route('dashboard.home') }}">{{ __('dashboard.settings.password.Dashboard') }}</a></li>
                <li><a href="{{ route('dashboard.settings') }}">{{ __('dashboard.settings.password.Settings') }}</a></li>
                <li>{{ __('dashboard.settings.password.Update password') }}</li>
            </ul>
        </nav>
    </div>

    <!-- Row -->

    @if ($errors->any())
        <div class="notification error closeable">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <a class="close"></a>
        </div>
    @endif

    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div id="test1" class="dashboard-box">

                <!-- Headline -->
                <div class="headline">
                    <h3><i class="icon-material-outline-lock"></i> {{ __('dashboard.settings.password.form.Password & Security') }}</h3>
                </div>

                <form id="settings-password" action="{{ route('dashboard.settings.password.update') }}" method="post">
                    @method('PUT')
                    @csrf

                    <div class="content with-padding">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="submit-field">
                                    <h5>{{ __('dashboard.settings.password.form.Current Password') }}</h5>
                                    <input type="password" name="password" class="with-border">
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="submit-field">
                                    <h5>{{ __('dashboard.settings.password.form.New Password') }}</h5>
                                    <input type="password" name="new_password" class="with-border">
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="submit-field">
                                    <h5>{{ __('dashboard.settings.password.form.Repeat New Password') }}</h5>
                                    <input type="password" name="new_password_confirmation" class="with-border">
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Button -->
        <div class="col-xl-12">
            <button type="submit" form="settings-password" class="button ripple-effect big margin-top-30">
                {{ __('dashboard.settings.password.form.Save Changes') }}
            </button>
        </div>

    </div>
    <!-- Row / End -->
@endsection
