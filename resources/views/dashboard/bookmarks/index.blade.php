@extends('layouts.dashboard')

@section('content')
    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
        <h3>{{ __('dashboard.settings.bookmarks.index.Bookmarks') }}</h3>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="{{ route('dashboard.home') }}">{{ __('dashboard.settings.bookmarks.index.Dashboard') }}</a></li>
                <li>{{ __('dashboard.settings.bookmarks.index.Bookmarks') }}</li>
            </ul>
        </nav>
    </div>

    <!-- Row -->
    <div class="row">

        <!-- Dashboard Box -->
        @if(count($jobs))
            <div class="col-xl-12">
                <div class="dashboard-box margin-top-0">

                    <!-- Headline -->
                    <div class="headline">
                        <h3><i class="icon-material-outline-business-center"></i> {{ __('dashboard.settings.bookmarks.index.Bookmarked Jobs') }}</h3>
                    </div>

                    <div class="content">
                        <ul class="dashboard-box-list">
                            @foreach($jobs as $job)
                                <li>
                                    <!-- Job Listing -->
                                    <div class="job-listing">

                                        <!-- Job Listing Details -->
                                        <div class="job-listing-details">

                                            <!-- Logo -->
                                            <a href="#" class="job-listing-company-logo">
                                                <img src="{{ $job->company->image() }}" alt="">
                                            </a>

                                            <!-- Details -->
                                            <div class="job-listing-description">
                                                <h3 class="job-listing-title">
                                                    <a href="{{ route('jobs.show', $job->id) }}">
                                                        {{ $job->title }}
                                                    </a>
                                                </h3>

                                                <!-- Job Listing Footer -->
                                                <div class="job-listing-footer">
                                                    <ul>
                                                        <li>
                                                            <i class="icon-material-outline-business"></i> {{ $job->company->name }}
                                                        </li>
                                                        <li>
                                                            <i class="icon-material-outline-location-on"></i> {{ $job->location ?? 'localisation non disponible' }}
                                                        </li>
                                                        <li>
                                                            <i class="icon-material-outline-business-center"></i> {{ $job->type }}
                                                        </li>
                                                        <li>
                                                            <i class="icon-material-outline-access-time"></i> {{ $job->created_at->diffForHumans() }}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Buttons -->
                                    {{--<div class="buttons-to-right">
                                        <a href="#" class="button red ripple-effect ico" title="Remove"
                                           data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                                    </div>--}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

    <!-- Dashboard Box -->
        @if(count($companies))
            <div class="col-xl-12">
                <div class="dashboard-box">

                    <!-- Headline -->
                    <div class="headline">
                        <h3><i class="icon-material-outline-location-city"></i> {{ __('dashboard.settings.bookmarks.index.Bookmarked Companies') }}</h3>
                    </div>

                    <div class="content">
                        <ul class="dashboard-box-list">
                            @foreach($companies as $company)
                                <li>
                                    <!-- Overview -->
                                    <div class="freelancer-overview">
                                        <div class="freelancer-overview-inner">

                                            <!-- Avatar -->
                                            <div class="freelancer-avatar">
                                                <div class="verified-badge"></div>
                                                <a href="{{ route('companies.show', $company->id) }}">
                                                    <img src="{{ $company->image() }}" alt=""></a>
                                            </div>

                                            <!-- Name -->
                                            <div class="freelancer-name">
                                                <h4><a href="{{ route('companies.show', $company->id) }}">{{ $company->name }}</a></h4>
                                                <span>{{ $company->title }}</span>
                                                <!-- Rating -->
                                                {{--<div class="freelancer-rating">
                                                    <div class="star-rating" data-rating="4.2"></div>
                                                </div>--}}
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Buttons -->
                                    {{--<div class="buttons-to-right">
                                        <a href="#" class="button red ripple-effect ico" title="Remove"
                                           data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                                    </div>--}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

    <!-- Dashboard Box -->
        @if(count($freelancers))
            <div class="col-xl-12">
                <div class="dashboard-box">

                    <!-- Headline -->
                    <div class="headline">
                        <h3><i class="icon-material-outline-face"></i> {{ __('dashboard.settings.bookmarks.index.Bookmarked Freelancers') }}</h3>
                    </div>

                    <div class="content">
                        <ul class="dashboard-box-list">
                            @foreach($freelancers as $user)
                                <li>
                                    <!-- Overview -->
                                    <div class="freelancer-overview">
                                        <div class="freelancer-overview-inner">

                                            <!-- Avatar -->
                                            <div class="freelancer-avatar">
                                                <div class="verified-badge"></div>
                                                <a href="{{ route('users.show', $user->id) }}">
                                                    <img src="{{ $user->image() }}" alt=""></a>
                                            </div>

                                            <!-- Name -->
                                            <div class="freelancer-name">
                                                <h4><a href="{{ route('users.show', $user->id) }}">{{ $user->name }}</a></h4>
                                                <span>{{ $user->title }}</span>
                                                <!-- Rating -->
                                                {{--<div class="freelancer-rating">
                                                    <div class="star-rating" data-rating="4.2"></div>
                                                </div>--}}
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Buttons -->
                                    {{--<div class="buttons-to-right">
                                        <a href="#" class="button red ripple-effect ico" title="Remove"
                                           data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                                    </div>--}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

    </div>
    <!-- Row / End -->
@endsection
