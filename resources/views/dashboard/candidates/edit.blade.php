@extends('layouts.dashboard')

@section('content')
    <div class="dashboard-headline">
        <h3>{{ $candidate->job->title }}</h3>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="{{ route('dashboard.home') }}">{{ __('dashboard.settings.candidates.index.Dashboard') }}</a></li>
                <li><a href="{{ route('dashboard.candidates.index') }}">{{ __('dashboard.settings.candidates.index.Applications') }}</a></li>
                <li>{{ $candidate->job->title }}</li>
            </ul>
        </nav>
    </div>

    @if ($errors->any())
        <div class="notification error closeable">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <a class="close"></a>
        </div>
    @endif

    @if(Session::has('message'))
        <div class="notification success closeable">
            <ul>
                <li>{{ Session::get('message') }}</li>
            </ul>
            <a class="close"></a>
        </div>
    @endif


    <div class="row">

        <div class="col-xl-12">
            <a href="{{ route('jobs.show', $candidate->job->id) }}" class="job-listing">

                <!-- Job Listing Details -->
                <div class="job-listing-details">
                    <!-- Logo -->
                    <div class="job-listing-company-logo">
                        <img src="{{ $candidate->job->company->image() }}" alt="">
                    </div>

                    <!-- Details -->
                    <div class="job-listing-description">
                        <h4 class="job-listing-company">{{ $candidate->job->company->name }}</h4>
                        <h3 class="job-listing-title">{{ $candidate->job->title }}</h3>
                    </div>
                </div>

                <!-- Job Listing Footer -->
                <div class="job-listing-footer">
                    <ul>
                        <li><i class="icon-material-outline-location-on"></i>{{ $candidate->job->location }}
                        </li>
                        <li><i class="icon-material-outline-business-center"></i>{{ $candidate->job->type }}
                        </li>
                        <li>
                            <i class="icon-material-outline-access-time"></i>{{ $candidate->job->created_at->diffForHumans() }}
                        </li>
                    </ul>
                </div>
            </a>
        </div>

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box">

                <!-- Headline -->
                <div class="headline">
                    <h3><i class="icon-material-outline-description"></i> {{ __('dashboard.settings.candidates.edit.Details') }}
                        <span>({{ __('dashboard.settings.candidates.edit.optional') }})</span></h3>
                </div>

                <div class="content">
                    <ul class="fields-ul">
                        <li>
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>{{ __('dashboard.settings.candidates.edit.Experience') }}
                                            <span>({{ __('dashboard.settings.candidates.edit.years') }})</span></h5>
                                        <input type="number" name="experience" form="settings-update"
                                               value="{{ $candidate->experience }}"
                                               class="with-border" placeholder="e.g. 6">
                                    </div>
                                </div>

                                <attachments class="col-xl-12"
                                             info="attachments for this job only"
                                             upload-route="{{ route('dashboard.candidates.attachments.add', $candidate->id) }}"
                                             remove-route="{{ route('dashboard.candidates.attachments.delete', '_uuid_') }}"
                                             :attachments="{{ $candidate->getMedia('attachments')->toJson() }}"></attachments>

                                <text-editor class="col-xl-12"
                                             name="description"
                                             title="{{ __('dashboard.settings.candidates.edit.Detials about your experience') }}"
                                             form-id="settings-update"
                                             content="{{ $candidate->description }}"
                                ></text-editor>

                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Button -->
        <div class="col-xl-12">
            <form id="settings-update" action="{{ route('dashboard.candidates.update',$candidate->id) }}" method="post"
                  enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <button type="submit" form="settings-update" class="button ripple-effect big margin-top-30">
                    {{ __('dashboard.settings.candidates.edit.Save Changes') }}
                </button>
                <button class="button dark ripple-effect big margin-top-30"
                        onclick="event.preventDefault();
                            document.getElementById('delete-form-{{ $candidate->id }}').submit();">
                    {{ __('dashboard.settings.candidates.edit.Delete') }}
                </button>
            </form>
            <form id="delete-form-{{ $candidate->id }}"
                  action="{{ route('dashboard.candidates.destroy', $candidate->id) }}"
                  method="POST"
                  style="display: none;">
                @csrf
                @method('DELETE')
            </form>
        </div>

    </div>
@endsection

@section('after-js')
    <script>
    localStorage.removeItem('applyToJob')
    </script>
@endsection
