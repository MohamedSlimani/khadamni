@extends('layouts.dashboard')

@section('content')
    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
        <h3>{{ __('dashboard.settings.candidates.index.Applications') }}</h3>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="dark">
            <ul>
                <li><a href="{{ route('dashboard.home') }}">{{ __('dashboard.settings.candidates.index.Dashboard') }}</a></li>
                <li>{{ __('dashboard.settings.candidates.index.Applications') }}</li>
            </ul>
        </nav>
    </div>

    <!-- Row -->
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">

                <!-- Headline -->
                <div class="headline">
                    <h3><i class="icon-material-outline-business-center"></i> {{ __('dashboard.settings.candidates.index.My Job Applications') }}</h3>
                </div>

                <div class="content">
                    <ul class="dashboard-box-list">
                        @if($candidates->count())
                            @foreach($candidates as $candidate)
                                <li>
                                    <!-- Job Listing -->
                                    <div class="job-listing">
                                        <!-- Job Listing Details -->
                                        <div class="job-listing-details">
                                            <!-- Details -->
                                            <div class="job-listing-description">
                                                <div class="job-listing-details">
                                                    <!-- Logo -->
                                                    <div class="job-listing-company-logo">
                                                        <img src="{{ $candidate->job->company->image() }}" alt="">
                                                    </div>

                                                    <!-- Details -->
                                                    <div class="job-listing-description">
                                                        <h4 class="job-listing-company">{{ $candidate->job->company->name }}</h4>
                                                        <h3 class="job-listing-title">{{ $candidate->job->title }}</h3>
                                                    </div>
                                                </div>

                                                <!-- Job Listing Footer -->
                                                <div class="job-listing-footer">
                                                    <ul>
                                                        <li>
                                                            <i class="icon-material-outline-assignment"></i>
                                                            {{ $candidate->job->type }}
                                                        </li>
                                                        <li>
                                                            <i class="icon-material-outline-business-center"></i>
                                                            {{ $candidate->job->category }}
                                                        </li>
                                                        <li>
                                                            <i class="icon-material-outline-date-range"></i>
                                                            {{ $candidate->job->created_at->format('Y/m/d') }}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Buttons -->
                                    <div class="buttons-to-right always-visible">
                                        <a href="{{ route('dashboard.candidates.edit', $candidate->id) }}"
                                           class="button gray ripple-effect ico" title="Edit"
                                           data-tippy-placement="top"><i class="icon-feather-edit"></i></a>
                                        <a href="#" class="button gray ripple-effect ico"
                                           title="Remove Candidate"
                                           onclick="event.preventDefault();
                                               document.getElementById('delete-form-{{ $candidate->id }}').submit();"
                                           data-tippy-placement="top">
                                            <i class="icon-feather-trash-2"></i>
                                        </a>
                                        <form id="delete-form-{{ $candidate->id }}"
                                              action="{{ route('dashboard.candidates.destroy', $candidate->id) }}"
                                              method="POST"
                                              style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <div class="companies-list d-flex align-items-center justify-content-center flex-column">
                                <div style="height: 287px">
                                    <img src="{{ asset('images/noresults.png') }}" alt="">
                                    <h3>{{ __('dashboard.settings.candidates.index.Nothing found') }}</h3>
                                </div>
                            </div>
                        @endif
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <!-- Row / End -->
@endsection
