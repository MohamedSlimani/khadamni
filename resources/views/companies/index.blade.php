@extends('layouts.app')

@section('content')

    <!-- Titlebar
    ================================================== -->
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>{{ __('app.footer.Browse Companies') }}</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('welcome') }}">{{ __('front.companies.index.Home') }}</a></li>
                            <li>{{ __('front.companies.index.Companies') }}</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <!-- Page Content
    ================================================== -->

    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <company-letters-filter />
            </div>
            <div class="col-xl-12">
                @if(count($companies))
                    <companies-list :companies="{{ $companies->toJson() }}"></companies-list>
                @else
                    <div class="companies-list d-flex align-items-center justify-content-center flex-column">
                        <div style="height: 287px">
                            <img src="{{ asset('images/noresults.png') }}" alt="">
                            <h3>{{ __('front.companies.index.Nothing found') }}</h3>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
