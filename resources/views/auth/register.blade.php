@extends('layouts.app')

@section('content')
    <!-- Titlebar
================================================== -->
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>{{ __('login.Register') }}</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('welcome') }}">{{ __('login.Home') }}</a></li>
                            <li>{{ __('login.Register') }}</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>


    <!-- Page Content
    ================================================== -->
    <div class="container">
        <div class="row">
            <div class="col-xl-5 offset-xl-3">

                <div class="login-register-page">
                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3 style="font-size: 26px;">{{ __('login.Let\'s create your account!') }}</h3>
                        <span>{{ __('login.Already have an account?') }} <a href="{{ route('login') }}">{{ __('login.Log In!') }}</a></span>
                    </div>

                    @if($errors->any())
                        <div class="notification error closeable">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <a class="close"></a>
                        </div>
                @endif

                <!-- Form -->
                    <form method="post" id="register-account-form" action="{{ route('register') }}">
                    @csrf
                        <input type="hidden" name="register" value="register">

                    <!-- Account Type -->
                        <div class="account-type">
                            <div>
                                <input type="radio" name="account-type-radio" id="freelancer-radio"
                                       class="account-type-radio" value="freelancer"
                                       checked />
                                <label for="freelancer-radio" class="ripple-effect-dark"><i
                                        class="icon-material-outline-account-circle"></i> {{ __('app.Freelancer') }}</label>
                            </div>

                            <div>
                                <input type="radio" name="account-type-radio" id="employer-radio"
                                       class="account-type-radio" value="employer" />
                                <label for="employer-radio" class="ripple-effect-dark"><i
                                        class="icon-material-outline-business-center"></i> {{ __('app.Employer') }}</label>
                            </div>
                        </div>

                        <div class="input-with-icon-left">
                            <i class="icon-material-outline-account-circle"></i>
                            <input type="text" class="input-text with-border" name="name"
                                   placeholder="{{ __('login.form.Full name') }}" required value="{{ old('name') }}" />
                        </div>

                        <div class="input-with-icon-left">
                            <i class="icon-material-baseline-mail-outline"></i>
                            <input type="text" class="input-text with-border" name="email"
                                   id="emailaddress-register" placeholder="{{ __('login.form.Email') }}" required {{ old('email') }} />
                        </div>

                        <div class="input-with-icon-left" title="Should be at least 8 characters long"
                             data-tippy-placement="bottom">
                            <i class="icon-material-outline-lock"></i>
                            <input type="password" class="input-text with-border" name="password"
                                   placeholder="{{ __('login.form.Password') }}" required />
                        </div>

                        <div class="input-with-icon-left">
                            <i class="icon-material-outline-lock"></i>
                            <input type="password" class="input-text with-border" name="password_confirmation"
                                   placeholder="{{ __('login.form.Repeat Password') }}" required />
                        </div>
                    </form>

                    <!-- Button -->
                    <button class="button full-width button-sliding-icon ripple-effect margin-top-10" type="submit"
                            form="register-account-form">{{ __('login.Register') }} <i class="icon-material-outline-arrow-right-alt"></i></button>

                    <!-- Social Login -->

                    {{--<div class="social-login-separator"><span>{{ __('login.or') }}</span></div>
                    <div class="social-login-buttons">
                        <button class="facebook-login ripple-effect"><i class="icon-brand-facebook"></i>
                            {{ __('login.Register via Facebook') }}
                        </button>
                        <button class="google-login ripple-effect"><i class="icon-brand-google"></i>
                            {{ __('login.Register via Google') }}
                        </button>
                    </div>--}}

                </div>

            </div>
        </div>
    </div>


    <!-- Spacer -->
    <div class="margin-top-70"></div>
    <!-- Spacer / End-->
@endsection

@section('after-js')
    <script>
    const url = document.location
    const params = new URLSearchParams(url.search.slice(1))

    if (params.has('job'))
        localStorage.setItem('applyToJob', params.get('job'))

    </script>
@endsection
