@extends('layouts.app')

@section('content')
    <!-- Titlebar
================================================== -->
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>{{ __('login.Log In') }}</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('welcome') }}">{{ __('login.Home') }}</a></li>
                            <li>{{ __('login.Log In') }}</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>


    <!-- Page Content
    ================================================== -->
    <div class="container">
        <div class="row">
            <div class="col-xl-5 offset-xl-3">

                <div class="login-register-page">
                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3>{{ __('login.We\'re glad to see you again!') }}</h3>
                        <span>{{ __('login.Don\'t have an account? ') }}<a href="{{ route('register') }}">{{ __('login.Sign Up!') }}</a></span>
                    </div>

                    @error('email')
                    <div class="notification error closeable">
                        <p>{{ $message }}</p>
                        <a class="close"></a>
                    </div>
                @enderror

                <!-- Form -->
                    <form method="post" id="login-form" action="{{ route('login') }}">
                        @csrf
                        <div class="input-with-icon-left">
                            <i class="icon-material-baseline-mail-outline"></i>
                            <input type="text" class="input-text with-border" name="email"
                                   placeholder="{{ __('login.form.Email') }}"
                                   value="{{ old('email') }}" required autocomplete="email" autofocus />
                        </div>

                        <div class="input-with-icon-left">
                            <i class="icon-material-outline-lock"></i>
                            <input type="password" class="input-text with-border"
                                   name="password" required autocomplete="current-password"
                                   placeholder="{{ __('login.form.Password') }}" />
                        </div>

                        <div class="checkbox">
                            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember"><span class="checkbox-icon"></span> {{ __('login.form.Remember Me') }}</label>
                        </div>
                    </form>

                    <!-- Button -->
                    <input type="hidden" form="login-form" name="apply_to_job" id="applyToJob">
                    <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="login-form">
                        {{ __('login.Log In') }}<i class="icon-material-outline-arrow-right-alt"></i>
                    </button>

                    <!-- Social Login -->
                    {{--
                    <div class="social-login-separator"><span>{{ __('login.or') }}</span></div>
                    <div class="social-login-buttons">
                        <button class="facebook-login ripple-effect"><i class="icon-brand-facebook"></i>
                            {{ __('login.Log In via Facebook') }}
                        </button>
                        <button class="google-login ripple-effect"><i class="icon-brand-google"></i>
                            {{ __('login.Log In via Google') }}
                        </button>
                    </div>--}}

                </div>

            </div>
        </div>
    </div>


    <!-- Spacer -->
    <div class="margin-top-70"></div>
    <!-- Spacer / End-->

@endsection

@section('after-js')
    <script>
    jobid = localStorage.getItem('applyToJob')
    if (jobid) {
        $('#applyToJob').val(jobid)
    }
    </script>
@endsection
