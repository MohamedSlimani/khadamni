<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- CSS
    ================================================== -->
    @include('layouts.partials.css')

</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

    <!-- Header Container
    ================================================== -->
@include('layouts.partials.header')
<!-- Header Container / End -->

@yield('content')

<!-- Footer
    ================================================== -->
@include('layouts.partials.footer')
<!-- Footer / End -->

</div>
<!-- Wrapper / End -->

@guest()
    <!-- Sign In Popup
================================================== -->
    @include('layouts.partials.sign-in-dialog')
    <!-- Sign In Popup / End -->
@endguest

<script>
let show_login
let show_register

@error('email')
    show_login = true
@enderror

    @if(old('register'))
    show_register = true
@endif
</script>

<!-- Scripts
================================================== -->
@yield('before-js')
@include('layouts.partials.js')
@yield('after-js')

</body>
</html>
