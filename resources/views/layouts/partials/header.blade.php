<header id="header-container" class="fullwidth">

    <!-- Header -->
    <div id="header">
        <div class="container">

            <!-- Left Side Content -->
            <div class="left-side">

                <!-- Logo -->
                <div id="logo">
                    <a href="{{ route('welcome') }}"><img src="{{ asset('images/logo.png') }}" alt=""></a>
                </div>

                <!-- Main Navigation -->
                <nav id="navigation">
                    <ul id="responsive">

                        <li><a href="#">{{ __('app.header.Find Work') }}</a>
                            <ul class="dropdown-nav">
                                <li>
                                    <a href="{{ route('jobs.index') }}">{{ __('app.header.Browse Jobs') }}</a>
                                </li>
                                <li><a href="{{ route('companies') }}">{{ __('app.header.Browse Companies') }}</a></li>
                            </ul>
                        </li>

                        <li><a href="#">{{ __('app.header.For Employers') }}</a>
                            <ul class="dropdown-nav">
                                <li><a href="{{ route('users.index') }}">{{ __('app.header.Find a Freelancer') }}</a></li>
                                <li><a href="{{ route('dashboard.jobs.create') }}">{{ __('app.header.Post a Job') }}</a></li>
                            </ul>
                        </li>
                            <!--
                        <li><a href="{{ route('how-it-works') }}">{{ __('app.header.How it works') }}</a></li>
                        <li><a href="{{ route('contact') }}">{{ __('app.header.Contact') }}</a></li>
                        -->
                    </ul>
                </nav>
                <div class="clearfix"></div>
                <!-- Main Navigation / End -->

            </div>
            <!-- Left Side Content / End -->

            <!-- Right Side Content / End -->
            @guest
                <div class="right-side">

                    <div class="header-widget">
                        <a href="#sign-in-dialog" id="log-in-button" class="popup-with-zoom-anim log-in-button"><i
                                class="icon-feather-log-in"></i> <span>{{ __('app.header.Log In / Register') }}</span></a>
                    </div>

                    <!-- Mobile Navigation Button -->
                    <span class="mmenu-trigger">
                        <button class="hamburger hamburger--collapse" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </span>

                </div>
            @else
                <div class="right-side">

                    <!--  User Notifications -->
                {{--@include('layouts.partials.notifications')--}}
                <!--  User Notifications / End -->

                    <!-- User Menu -->
                @include('layouts.partials.user-menu')
                <!-- User Menu / End -->

                    <!-- Mobile Navigation Button -->
                    <span class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>

                </div>
        @endguest
        <!-- Right Side Content / End -->

        </div>
    </div>
    <!-- Header / End -->

</header>
<div class="clearfix"></div>
