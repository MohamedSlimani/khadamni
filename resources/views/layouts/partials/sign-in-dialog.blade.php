<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#login">{{ __('login.Log In') }}</a></li>
            <li><a href="#register">{{ __('login.Register') }}</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Login -->
            <div class="popup-tab-content" id="login">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>{{ __('login.We\'re glad to see you again!') }}</h3>
                    <span>{{ __('login.Don\'t have an account? ') }} <a href="#" class="register-tab">{{ __('login.Sign Up!') }}</a></span>
                </div>

                @error('email')
                <div class="notification error closeable">
                    <p>{{ $message }}</p>
                    <a class="close"></a>
                </div>
                @enderror

            <!-- Form -->
                <form method="post" id="login-form" action="{{ route('login') }}">
                    @csrf
                    <div class="input-with-icon-left">
                        <i class="icon-material-baseline-mail-outline"></i>
                        <input type="text" class="input-text with-border" name="email"
                               placeholder="{{ __('login.form.Email') }}"
                               value="{{ old('email') }}" required autocomplete="email" autofocus />
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-material-outline-lock"></i>
                        <input type="password" class="input-text with-border"
                               name="password" required autocomplete="current-password"
                               placeholder="{{ __('login.form.Password') }}" />
                    </div>

                    <div class="checkbox">
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label for="remember"><span class="checkbox-icon"></span> {{ __('login.form.Remember Me') }}</label>
                    </div>
                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="login-form">Log
                    In <i class="icon-material-outline-arrow-right-alt"></i></button>
<!--
                <div class="margin-top-30">
                    <a href="#" class="forgot-password">Forgot Password?</a>
                </div>
-->
                {{--<!-- Social Login -->
                <div class="social-login-separator"><span>{{ __('login.or') }}</span></div>
                <div class="social-login-buttons">
                    <button class="facebook-login ripple-effect"><i class="icon-brand-facebook"></i>
                        {{ __('login.Log In via Facebook') }}
                    </button>
                    <button class="google-login ripple-effect"><i class="icon-brand-google"></i>
                        {{ __('login.Log In via Google') }}
                    </button>
                </div>--}}

            </div>

            <!-- Register -->
            <div class="popup-tab-content" id="register">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>{{ __('login.Let\'s create your account!') }}</h3>
                </div>

                @if ($errors->any())
                    <div class="notification error closeable">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <a class="close"></a>
                    </div>
                @endif

                <!-- Form -->
                <form method="post" id="register-account-form" action="{{ route('register') }}">
                @csrf
                    <input type="hidden" name="register" value="register">
                <!-- Account Type -->
                    <div class="account-type">
                        <div>
                            <input type="radio" name="account-type-radio" id="freelancer-radio"
                                   class="account-type-radio" value="freelancer"
                                   checked />
                            <label for="freelancer-radio" class="ripple-effect-dark"><i
                                    class="icon-material-outline-account-circle"></i> {{ __('app.Freelancer') }}</label>
                        </div>

                        <div>
                            <input type="radio" name="account-type-radio" id="employer-radio"
                                   class="account-type-radio" value="employer" />
                            <label for="employer-radio" class="ripple-effect-dark"><i
                                    class="icon-material-outline-business-center"></i> {{ __('app.Employer') }}</label>
                        </div>
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-material-outline-account-circle"></i>
                        <input type="text" class="input-text with-border" name="name"
                               placeholder="{{ __('login.form.Full name') }}" required />
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-material-baseline-mail-outline"></i>
                        <input type="text" class="input-text with-border" name="email"
                               id="emailaddress-register" placeholder="{{ __('login.form.Email') }}" required />
                    </div>

                    <div class="input-with-icon-left" title="Should be at least 8 characters long"
                         data-tippy-placement="bottom">
                        <i class="icon-material-outline-lock"></i>
                        <input type="password" class="input-text with-border" name="password"
                               placeholder="{{ __('login.form.Password') }}" required />
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-material-outline-lock"></i>
                        <input type="password" class="input-text with-border" name="password_confirmation"
                               placeholder="{{ __('login.form.Repeat Password') }}" required />
                    </div>
                </form>

                <!-- Button -->
                <button class="margin-top-10 button full-width button-sliding-icon ripple-effect" type="submit"
                        form="register-account-form">{{ __('login.Register') }} <i class="icon-material-outline-arrow-right-alt"></i>
                </button>

                {{--<!-- Social Login -->
                <div class="social-login-separator"><span>{{ __('login.or') }}</span></div>
                <div class="social-login-buttons">
                    <button class="facebook-login ripple-effect"><i class="icon-brand-facebook"></i>
                        {{ __('login.Register via Facebook') }}
                    </button>
                    <button class="google-login ripple-effect"><i class="icon-brand-google"></i>
                        {{ __('login.Register via Google') }}
                    </button>
                </div>--}}

            </div>

        </div>
    </div>
</div>
