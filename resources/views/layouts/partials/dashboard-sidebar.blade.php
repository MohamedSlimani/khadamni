@php
    $route = Route::currentRouteName();
@endphp

<div class="dashboard-sidebar">
    <div class="dashboard-sidebar-inner" data-simplebar>
        <div class="dashboard-nav-container">

            <!-- Responsive Navigation Trigger -->
            <a href="#" class="dashboard-responsive-nav-trigger">
					<span class="hamburger hamburger--collapse">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
                <span class="trigger-title">{{ __('dashboard.sidebar.Dashboard Navigation') }}</span>
            </a>

            <!-- Navigation -->
            <div class="dashboard-nav">
                <div class="dashboard-nav-inner">

                    {{--<ul data-submenu-title="Start">
                        <li class="{{ $route == 'dashboard.home' ? 'active' : '' }}"><a
                                href="{{ route('dashboard.home') }}"><i
                                    class="icon-material-outline-dashboard"></i> Dashboard</a></li>
                    </ul>--}}

                    <ul data-submenu-title="{{ __('dashboard.sidebar.Organize and Manage') }}">
                        <li class="{{ $route == 'dashboard.bookmarks.index' ? 'active' : '' }}">
                            <a href="{{ route('dashboard.bookmarks.index') }}">
                                <i class="icon-material-outline-star-border"></i> {{ __('dashboard.sidebar.Bookmarks') }}</a>
                        </li>
                        @if(Auth::user()->employer)
                            <li class="{{ $route == 'dashboard.jobs.create' || $route == 'dashboard.jobs.index' || $route == 'dashboard.jobs.edit' ? 'active-submenu' : '' }}">
                                <a href="#"><i class="icon-material-outline-business-center"></i> {{ __('dashboard.sidebar.Jobs') }}</a>
                                <ul>
                                    <li class="{{ $route == 'dashboard.jobs.index' || $route == 'dashboard.jobs.edit' ? 'active-submenu' : '' }}">
                                        <a href="{{ route('dashboard.jobs.index') }}"> {{ __('dashboard.sidebar.Manage Jobs') }}
                                            <span class="nav-tag">{{ Auth::user()->jobs->count() }}</span>
                                        </a>
                                    </li>
                                    <li class="{{ $route == 'dashboard.jobs.create' ? 'active' : '' }}">
                                        <a href="{{ route('dashboard.jobs.create') }}">{{ __('dashboard.sidebar.Post a Job') }}</a>
                                    </li>
                                </ul>
                            </li>
                        @else
                            <li class="{{ $route == 'dashboard.candidates.edit' || $route == 'dashboard.candidates.index' ? 'active' : '' }}">
                                <a href="{{ route('dashboard.candidates.index') }}"><i class="icon-material-outline-business-center"></i> {{ __('dashboard.sidebar.Jobs Applications') }}</a>
                            </li>

                        @endif
                    </ul>

                    <ul data-submenu-title="{{ __('dashboard.sidebar.Account') }}">
                        <li class="{{ $route == 'dashboard.settings' || $route == 'dashboard.settings.password' ? 'active-submenu' : '' }}">
                            <a href="#"><i class="icon-material-outline-settings"></i> {{ __('dashboard.sidebar.Settings') }}</a>
                            <ul>
                                <li class="{{ $route == 'dashboard.settings' ? 'active' : '' }}">
                                    <a href="{{ route('dashboard.settings') }}">{{ __('dashboard.sidebar.Update Account') }}</a>
                                </li>
                                <li class="{{ $route == 'dashboard.settings.password' ? 'active' : '' }}">
                                    <a href="{{ route('dashboard.settings.password') }}"> {{ __('dashboard.sidebar.Update Password') }}</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form-sidbar').submit();">
                                <i class="icon-material-outline-power-settings-new"></i> {{ __('dashboard.sidebar.Logout') }}</a></li>
                    </ul>
                    <form id="logout-form-sidbar" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </div>
            </div>
            <!-- Navigation / End -->

        </div>
    </div>
</div>
