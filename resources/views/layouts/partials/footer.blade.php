<div id="footer">

    <!-- Footer Top Section -->
    <div class="footer-top-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <!-- Footer Rows Container -->
                    <div class="footer-rows-container">

                        <!-- Left Side -->
                        <div class="footer-rows-left">
                            <div class="footer-row">
                                <div class="footer-row-inner footer-logo">
                                    <img src="{{ asset('images/logo2.png') }}" alt="">
                                </div>
                            </div>
                        </div>

                        <!-- Right Side -->
                        <div class="footer-rows-right">
                            
                            <!-- Social Icons -->
                            <!--
                            <div class="footer-row">
                                <div class="footer-row-inner">
                                    <ul class="footer-social-links">
                                        <li>
                                            <a href="#" title="Facebook" data-tippy-placement="bottom"
                                               data-tippy-theme="light">
                                                <i class="icon-brand-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="Twitter" data-tippy-placement="bottom"
                                               data-tippy-theme="light">
                                                <i class="icon-brand-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="Google Plus" data-tippy-placement="bottom"
                                               data-tippy-theme="light">
                                                <i class="icon-brand-google-plus-g"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="LinkedIn" data-tippy-placement="bottom"
                                               data-tippy-theme="light">
                                                <i class="icon-brand-linkedin-in"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
-->
                            <!-- Language Switcher -->
                            <div class="footer-row">
                                <div class="footer-row-inner">
                                    <select class="selectpicker language-switcher" onchange="location.href = '/' + this.value;" data-selected-text-format="count"
                                            data-size="5">
                                        <option {{ app()->getLocale() != "fr" ? 'selected': '' }} value="en">English</option>
                                        <option {{ app()->getLocale() == "fr" ? 'selected': '' }} value="fr">Français</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Footer Rows Container / End -->
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Top Section / End -->

    <!-- Footer Middle Section -->
    <div class="footer-middle-section">
        <div class="container">
            <div class="row">

                <!-- Links -->
                <div class="col-xl-2 col-lg-2 col-md-3">
                    <div class="footer-links">
                        <h3>{{ __('app.footer.For Candidates') }}</h3>
                        <ul>
                            <li><a href="{{ route('jobs.index') }}"><span>{{ __('app.footer.Browse Jobs') }}</span></a></li>
                            <li><a href="{{ route('companies') }}"><span>{{ __('app.footer.Browse Companies') }}</span></a></li>
                            <li><a href="{{ route('dashboard.settings') }}"><span>{{ __('app.footer.Add Resume') }}</span></a></li>
                            <li><a href="{{ route('dashboard.bookmarks.index') }}"><span>{{ __('app.footer.My Bookmarks') }}</span></a></li>
                        </ul>
                    </div>
                </div>

                <!-- Links -->
                <div class="col-xl-2 col-lg-2 col-md-3">
                    <div class="footer-links">
                        <h3>{{ __('app.footer.For Employers') }}</h3>
                        <ul>
                            <li><a href="{{ route('users.index') }}"><span>{{ __('app.footer.Find a Freelancer') }}</span></a></li>
                            <li><a href="{{ route('dashboard.jobs.create') }}"><span>{{ __('app.footer.Post a Job') }}</span></a></li>
                        </ul>
                    </div>
                </div>

                <!-- Links -->
                <!--
                <div class="col-xl-2 col-lg-2 col-md-3">
                    <div class="footer-links">
                        <h3>{{ __('app.footer.Helpful Links') }}</h3>
                        <ul>
                            <li><a href="{{ route('contact') }}"><span>{{ __('app.footer.Contact') }}</span></a></li>
                            <li><a href="{{ route('how-it-works') }}"><span>{{ __('app.footer.How it works') }}</span></a></li>
                            <li><a href="#"><span>{{ __('app.footer.Privacy Policy') }}</span></a></li>
                            <li><a href="#"><span>{{ __('app.footer.Terms of Use') }}</span></a></li>
                        </ul>
                    </div>
                </div>
-->
                <!-- Links -->
                <div class="col-xl-2 col-lg-2 col-md-3">
                    <div class="footer-links">
                        <h3>{{ __('app.footer.Account') }}</h3>
                        <ul>
                            <li><a href="{{ route('login') }}"><span>{{ __('app.footer.Log In') }}</span></a></li>
                            <li><a href="{{ route('dashboard.settings') }}"><span>{{ __('app.footer.My Account') }}</span></a></li>
                        </ul>
                    </div>
                </div>

                <!-- Newsletter -->
                <div class="col-xl-4 col-lg-4 col-md-12">
                    <h3><i class="icon-feather-mail"></i> {{ __('app.footer.Subscribe') }}</h3>
                    <p>{{ __('app.footer.Subscribe text') }}</p>
                    <form action="#" method="get" class="newsletter">
                        <input type="text" name="fname" placeholder="{{ __('app.footer.Subscribe placeholder') }}">
                        <button type="submit"><i class="icon-feather-arrow-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Middle Section / End -->

    <!-- Footer Copyrights -->
    <div class="footer-bottom-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    © {{ date('Y') }} <strong>{{ config('app.name', 'Laravel') }}</strong>. All Rights Reserved.
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Copyrights / End -->

</div>
