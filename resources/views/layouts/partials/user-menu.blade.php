<div class="header-widget">
    <div class="header-notifications user-menu">
        <div class="header-notifications-trigger">
            <a href="#">
                <div class="user-avatar status-online">
                    <img src="{{ Auth::user()->image() ?? asset('images/user-avatar-placeholder.png') }}"
                         alt="">
                </div>
            </a>
        </div>

        <!-- Dropdown -->
        <div class="header-notifications-dropdown">

            <!-- User Status -->
            <div class="user-status">

                <!-- User Name / Avatar -->
                <div class="user-details">
                    <div class="user-avatar status-online"><img src="{{ Auth::user()->image() ?? asset('images/user-avatar-placeholder.png') }}"
                                                                alt=""></div>
                    <div class="user-name">
                        {{ Auth::user()->name }} <span>{{ Auth::user()->employer ? __('app.Employer') : __('app.Freelancer') }}</span>
                    </div>
                </div>

                {{--<!-- User Status Switcher -->
                <div class="status-switch" id="snackbar-user-status">
                    <label class="user-online current-status">Online</label>
                    <label class="user-invisible">Invisible</label>
                    <!-- Status Indicator -->
                    <span class="status-indicator" aria-hidden="true"></span>
                </div>--}}
            </div>

            <ul class="user-menu-small-nav">
                <li><a href="{{ route('dashboard.home') }}"><i class="icon-material-outline-dashboard"></i>
                        {{ __('app.Routes.Dashboard') }}</a></li>
                <li><a href="{{ route('dashboard.settings') }}"><i class="icon-material-outline-settings"></i>
                        {{ __('app.Routes.Settings') }}</a></li>
                <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="icon-material-outline-power-settings-new"></i> {{ __('Logout') }}</a></li>
            </ul>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

        </div>
    </div>

</div>
