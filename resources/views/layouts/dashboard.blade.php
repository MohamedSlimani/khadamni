<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- CSS
    ================================================== -->
    @include('layouts.partials.css')

</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

    <!-- Header Container
    ================================================== -->
@include('layouts.partials.header')
<!-- Header Container / End -->

    <!-- Dashboard Container -->
    <div class="dashboard-container">

        <!-- Dashboard Sidebar
        ================================================== -->
        @include('layouts.partials.dashboard-sidebar')
        <!-- Dashboard Sidebar / End -->

        <!-- Dashboard Content
        ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner">

            @yield('content')

            <!-- Footer -->
            @include('layouts.partials.dashboard-footer')
            <!-- Footer / End -->

            </div>
        </div>
        <!-- Dashboard Content / End -->

    </div>
    <!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->

@guest()
    <!-- Sign In Popup
================================================== -->
    @include('layouts.partials.sign-in-dialog')
    <!-- Sign In Popup / End -->
@endguest

<script>
let show_login
let show_register

@error('email')
    show_login = true
@enderror

    @if(old('register'))
    show_register = true
@endif
</script>

<!-- Scripts
================================================== -->
<script>
@if(Session::has('message'))
var successMessage = '{{ Session::get('message') }}'
@endif
</script>
@yield('before-js')
@include('layouts.partials.js')
@yield('after-js')

</body>
</html>
