<div class="full-page-sidebar">
    <div class="full-page-sidebar-inner" data-simplebar>
        <div class="sidebar-container">

            <!-- Location -->
            <locations class="sidebar-widget"
                       form-id="user-search-form"
                     
                       no-map
            ></locations>

            <!-- Keywords -->
            <tags-editor form-id="user-search-form" name="keywords" title="Keywords" placeholder="e.g. name or title"></tags-editor>

            {{--<!-- Tags -->
            <div class="sidebar-widget">
                <h3>Skills</h3>

                <div class="tags-container">
                    <div class="tag">
                        <input type="checkbox" id="tag1"/>
                        <label for="tag1">front-end dev</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag2"/>
                        <label for="tag2">angular</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag3"/>
                        <label for="tag3">react</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag4"/>
                        <label for="tag4">vue js</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag5"/>
                        <label for="tag5">web apps</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag6"/>
                        <label for="tag6">design</label>
                    </div>
                    <div class="tag">
                        <input type="checkbox" id="tag7"/>
                        <label for="tag7">wordpress</label>
                    </div>
                </div>
                <div class="clearfix"></div>

                <!-- More Skills -->
                <div class="keywords-container margin-top-20">
                    <div class="keyword-input-container">
                        <input type="text" class="keyword-input" placeholder="add more skills"/>
                        <button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
                    </div>
                    <div class="keywords-list"><!-- keywords go here --></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>--}}

            <div class="margin-bottom-40"></div>

        </div>
        <!-- Sidebar Container / End -->

        <!-- Search Button -->
        <div class="sidebar-search-button-container">
            <form id="user-search-form">
                <button type="submit" class="button ripple-effect">Search</button>
            </form>
        </div>
        <!-- Search Button / End-->

    </div>
</div>
