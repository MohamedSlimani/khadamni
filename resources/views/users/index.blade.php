@extends('layouts.app-no-footer')

@section('content')
    <!-- Page Content
================================================== -->
    <div class="full-page-container">

    @include('users.partials.sidebar')

    <!-- Full Page Content -->
        <div class="full-page-content-container" data-simplebar>
            <div class="full-page-content-inner">

                <h3 class="page-title">{{ __('front.users.index.Search Results') }}</h3>

                {{--<div class="notify-box margin-top-15">
                    <div class="switch-container">
                        <label class="switch"><input type="checkbox"><span class="switch-button"></span><span
                                class="switch-text">Turn on email alerts for this search</span></label>
                    </div>

                    <div class="sort-by">
                        <span>Sort by:</span>
                        <select class="selectpicker hide-tick">
                            <option>Relevance</option>
                            <option>Newest</option>
                            <option>Oldest</option>
                            <option>Random</option>
                        </select>
                    </div>
                </div>--}}

                <!-- Freelancers List Container -->
                <div class="freelancers-container freelancers-grid-layout margin-top-35">

                    <!--Freelancer -->
                    @if($users->count())
                        @foreach($users as $user)
                            <div class="freelancer">

                                <!-- Overview -->
                                <div class="freelancer-overview">
                                    <div class="freelancer-overview-inner">

                                        <!-- Bookmark Icon -->
                                        @auth()
                                            <book-mark id="{{ $user->freelancerBookmark() ? $user->freelancerBookmark()->id : '' }}"
                                                       model-id="{{ $user->id }}"
                                                       model-type="freelancer"
                                                       add-route="{{ route('dashboard.bookmarks.store') }}"
                                                       remove-route="{{ route('dashboard.bookmarks.destroy','_id_' ) }}"
                                            ></book-mark>
                                        @endauth

                                        <!-- Avatar -->
                                        <div class="freelancer-avatar">
                                            {{--<div class="verified-badge"></div>--}}
                                            <a href="{{ route('users.show', $user->id) }}"><img
                                                    src="{{ $user->image() }}" alt=""></a>
                                        </div>

                                        <!-- Name -->
                                        <div class="freelancer-name">
                                            <h4><a href="{{ route('users.show', $user->id) }}">
                                                    {{ $user->name }}
                                                </a>
                                            </h4>
                                            <span>{{ $user->title }}</span>
                                        </div>

                                        <!-- Rating -->
                                        {{--<div class="freelancer-rating">
                                            <div class="star-rating" data-rating="4.9"></div>
                                        </div>--}}
                                    </div>
                                </div>

                                <!-- Details -->
                                <div class="freelancer-details">
                                    <div class="freelancer-details-list">
                                        <ul>
                                            <li>{{ __('front.users.index.Location') }} <strong><i class="icon-material-outline-location-on"></i>
                                                    {{ $user->location ?? 'Localisation non disponible' }}</strong></li>
                                        </ul>
                                    </div>
                                    <a href="{{ route('users.show', $user->id) }}"
                                       class="button button-sliding-icon ripple-effect">{{ __('front.users.index.View Profile') }} <i
                                            class="icon-material-outline-arrow-right-alt"></i></a>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="companies-list d-flex align-items-center justify-content-center flex-column">
                            <div style="height: 287px">
                                <img src="{{ asset('images/noresults.png') }}" alt="">
                                <h3>{{ __('front.users.index.Nothing found') }}</h3>
                            </div>
                        </div>
                    @endif
                    <!-- Freelancer / End -->

                </div>
                <!-- Freelancers Container / End -->

                <!-- Pagination -->
                <div class="clearfix"></div>
                {{ $users->links('layouts.pagination.dafault') }}
                <div class="clearfix"></div>
                <!-- Pagination / End -->

                <!-- Footer -->
                @include('layouts.partials.small-footer')
                <!-- Footer / End -->

            </div>
        </div>
        <!-- Full Page Content / End -->

    </div>
@endsection

@section('before-js')
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ config('google-maps.key') }}&libraries=places"></script>
@endsection
