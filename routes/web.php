<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@welcome')->name('welcome');

Route::get('/how-it-works', function () {
    return view('how-it-works');
})->name('how-it-works');

Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Auth::routes();


Route::get('companies', 'CompanyController@index')->name('companies');
Route::get('companies/{company}', 'CompanyController@show')->name('companies.show');
Route::get('/download/{uuid}', 'MediaController@download')->name('download');


Route::resource('jobs','FrontJobsController');
Route::resource('users', 'FrontUsersController');

Route::get('fr', function() {
    cookie()->forever('locale','fr');
    return redirect()->back()->cookie('locale', 'fr', 5256000);
});

Route::get('en', function() {
    return redirect()->back()->cookie('locale', 'en', 5256000);
});

Route::get('lang/{key}', function ($key) {
   return Lang::get('components.'.$key);
});


Route::middleware('auth')->prefix('dashboard')->name('dashboard.')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/settings', 'UserController@account')->name('settings');
    Route::get('/settings/password', 'UserController@password')->name('settings.password');
    Route::put('/settings', 'UserController@update')->name('settings.update');
    Route::put('/settings/password', 'UserController@updatePassword')->name('settings.password.update');
    Route::post('/settings/attachments', 'UserController@uploadAttachment')->name('settings.upload');
    Route::delete('/settings/attachments/{uuid}', 'UserController@removeAttachment')->name('attachments.remove');


    Route::resource('jobs','JobController')->middleware('type:employer');
    Route::post('jobs/attachments/temp','JobController@tempAttachments')->name('jobs.attachments.temp');
    Route::post('jobs/{id}/attachments/temp','JobController@addAttachments')->name('jobs.attachments.add');
    Route::delete('jobs/attachments/delete/{uuid}','JobController@removeAttachment')->name('jobs.attachments.delete');

    Route::get('jobs/{job}/candidates','JobController@candidates')->name('jobs.candidates');
    Route::get('jobs/{job}/candidates/{candidate}','JobController@showCandidates')->name('jobs.candidates.show');
    Route::delete('jobs/{job}/candidates/{candidate}','JobController@deleteCandidates')->name('jobs.candidates.destroy');

    Route::resource('candidates', 'CandidateController')->middleware('type:freelancer');

    Route::post('candidates/{id}/attachments/temp','CandidateController@addAttachments')->name('candidates.attachments.add');
    Route::delete('candidates/attachments/delete/{uuid}','CandidateController@removeAttachment')->name('candidates.attachments.delete');

    Route::resource('bookmarks', 'BookmarkController');
});
