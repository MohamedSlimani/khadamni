<?php

namespace App\Http\Controllers;

use App\Bookmark;
use App\Job;
use App\User;
use Illuminate\Http\Request;

class BookmarkController extends Controller
{

    public function index()
    {
        $user = User::find(\Auth::id());
        $bookmarks = $user->bookmarks()->paginate();
        $jobs = [];
        $companies = [];
        $freelancers = [];

        foreach ($bookmarks as $bookmark) {
            if($bookmark->job) array_push($jobs, $bookmark->job);
            else if($bookmark->company) array_push($companies, $bookmark->company);
            else if($bookmark->freelancer) array_push($freelancers, $bookmark->freelancer);
        }

        return view('dashboard.bookmarks.index', compact('freelancers', 'companies', 'jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if ($request->has('model_type') && $request->has('model_id')) {
            $bookmark = new Bookmark(['user_id' => $request->user()->id]);
            switch ($request['model_type']) {
                case 'job':
                    $job = Job::find($request->get('model_id'));
                    if ($job) {
                        $bookmark->job()->associate($job);
                        $bookmark->save();
                        return  $bookmark;
                    } else {
                        return abort(400);
                    }
                    break;
                case 'freelancer':
                    $freelancer = User::find($request->get('model_id'));
                    if ($freelancer) {
                        $bookmark->freelancer()->associate($freelancer);
                        $bookmark->save();
                        return  $bookmark;
                    } else {
                        return abort(400);
                    }
                    break;
                case 'company':
                    $company = User::find($request->get('model_id'));
                    if ($company) {
                        $bookmark->company()->associate($company);
                        $bookmark->save();
                        return  $bookmark;
                    } else {
                        return abort(400);
                    }
                default: return abort(400);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Bookmark $bookmark
     * @return \Illuminate\Http\Response
     */
    public function show(Bookmark $bookmark)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Bookmark $bookmark
     * @return \Illuminate\Http\Response
     */
    public function edit(Bookmark $bookmark)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Bookmark $bookmark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bookmark $bookmark)
    {
        //
    }

    public function destroy(Bookmark $bookmark)
    {
        try {
            $bookmark->delete();
            return response()->noContent();
        } catch (\Exception $e) {
        }
    }
}
