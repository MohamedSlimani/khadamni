<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Job;
use Auth;
use Illuminate\Http\Request;

class FrontJobsController extends Controller
{
    public function index()
    {


        $query = Job::customSearchQuery(request()->toArray());


        if (request()->has('sortBy')) {
            switch (request('sortBy')) {
                case 'old':
                    $query = $query->orderBy('created_at');
                    break;
                default:
                    $query = $query->orderBy('created_at', 'DESC');
                    break;
            }
        }

        $jobs = Job::setQuery($query)->paginate();


        foreach ($jobs as $job) {
            $job->url = route('jobs.show', $job->id);
            $job->logo = $job->company->image();
            $job->companyName = $job->company->name;
        }

        $categories = [
            'Agriculture, Food and Natural Resources',
            'Architecture and Construction',
            'Arts, Audio/Video Technology and Communications',
            'Business Management and Administration',
            'Education and Training',
            'Finance',
            'Government and Public Administration',
            'Health Science',
            'Hospitality and Tourism',
            'Human Services',
            'Information Technology',
            'Law, Public Safety, Corrections and Security',
            'Manufacturing',
            'Marketing, Sales and Service',
            'Science, Technology, Engineering and Mathematics'
        ];

        $types = ['Freelance', 'Full Time', 'Part Time', 'Internship', 'Temporary'];

        return view('jobs.index', compact('jobs', 'categories', 'types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        $job = Job::findOrFail($id);

        if (Auth::user())
            $candidate = Auth::user()->candidates->where('job_id', $job->id)->first();
        else {
            $candidate = null;
        }

        return view('jobs.show', compact('job', 'candidate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
