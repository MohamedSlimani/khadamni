<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index()
    {
        $query = User::where('employer', 1)->limit(12);

        if (request()->has('letter'))
            $query->where('name', 'like', request()->get('letter') . '%');


        $companies = $query->get();
        foreach ($companies as $company) {
            $company->logo = $company->image();
            $company->url = route('companies.show', $company);
        }

        return view('companies.index', compact('companies', ));
    }

    public function show(User $company)
    {
        if ($company->employer) {
            $jobs = $company->jobs()->latest()->paginate(3, ['*'], 'jobs');
            return view('companies.show', compact('company', 'jobs'));
        }
        else {
            return abort(404);
        }
    }
}
