<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Job;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class CandidateController extends Controller
{
    public function index()
    {
        $user = User::find(Auth::id());
        $candidates = $user->candidates()->latest()->paginate(8);
        return view('dashboard.candidates.index',compact('candidates'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $user = User::find(Auth::id());
        $job = Job::findOrFail($request->get('job'));

        $candidate = $user->candidates()->where('job_id',$job->id)->first();

        if(!$candidate) {
            $candidate = Candidate::create([
                'user_id' => $user->id,
                'job_id' => $job->id
            ]);
        }

        foreach ($user->getMedia('attachments') as $media) {
            $media->copy($candidate, 'attachments');
        }

        return redirect()->route('dashboard.candidates.edit', $candidate->id)
            ->with('message', 'Application submitted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Candidate $candidate
     * @return \Illuminate\Http\Response
     */
    public function show(Candidate $candidate)
    {
        //
    }


    public function edit(Candidate $candidate)
    {
        Auth::user()->candidates()->findOrFail($candidate->id);
        return view('dashboard.candidates.edit', compact('candidate'));
    }


    public function update(Request $request, Candidate $candidate)
    {
        Auth::user()->candidates()->findOrFail($candidate->id);
        $validated_data = $request->validate([
            'experience' => 'nullable|string',
            'description' => 'nullable|string',
        ]);

        $candidate->update($validated_data);

        return redirect()->route('dashboard.candidates.edit', $candidate->id)
            ->with('message', 'Application submitted successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Candidate $candidate
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Candidate $candidate)
    {
        try {
            Auth::user()->candidates()->findOrFail($candidate->id)->delete();
            return redirect()->route('dashboard.candidates.index')->with('message', 'candidate deleted');
        } catch (\Exception $e) {
        }
    }

    public function addAttachments($id, Request $request)
    {
        $user = User::find(Auth::id());
        $job = $user->candidates->find($id);
        try {
            $file = $job->addMediaFromRequest('file')
                ->toMediaCollection('attachments');
            return response()->json($file);
        } catch (FileDoesNotExist $e) {
        } catch (FileIsTooBig $e) {
        }
        return null;
    }

    public function removeAttachment($uuid)
    {
        $media = Media::findByUuid($uuid);
        $user = User::find(Auth::id());

        try {
            if ($user->candidates->find($media->getAttribute('model_id')))
                if ($media->delete())
                    return response()->noContent();
            return 'nop!';
        } catch (\Exception $e) {
        }

        return null;
    }
}
