<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function welcome()
    {
        $numbers = [];
        $numbers['jobs'] = \DB::table('jobs')->count();
        $numbers['freelancers'] = \DB::table('users')->where('freelancer', 1)->count();
        $numbers['employer'] = \DB::table('users')->where('employer', 1)->count();

        $jobs = Job::latest()->limit(5)->get();

        return view('welcome', compact('numbers', 'jobs'));
    }
}
