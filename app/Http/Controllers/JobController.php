<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Job;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class JobController extends Controller
{
    public function index()
    {
        $jobs = Auth::user()->jobs;
        return view('dashboard.jobs.index', compact('jobs'));
    }

    public function create()
    {
        return view('dashboard.jobs.create');
    }


    public function store(Request $request)
    {
        $validated_data = $request->validate([
            'title' => 'required|string',
            'type' => 'required|string',
            'category' => 'required|string',
            'location' => 'nullable|string',
            'lat' => 'nullable|string',
            'lng' => 'nullable|string',
            'map_zoom' => 'nullable|string',
            'salary_min' => 'nullable|string',
            'salary_max' => 'nullable|string',
            'tags' => 'nullable|string',
            'description' => 'required|string',
        ]);

        $user = User::find(Auth::id());

        $validated_data["tags"] = explode(',', $validated_data["tags"]);

        $job = Auth::user()->jobs()->create($validated_data);

        foreach ($user->getMedia('tempJobsAttachments') as $media) {
            $media->copy($job, 'attachments');
            $media->delete();
        }

        return redirect()->route('dashboard.jobs.edit', $job->id)->with('message', 'job created successfully');
    }


    public function show(Job $job)
    {
        $job->getMedia();
        return $job;
    }


    public function edit(Job $job)
    {
        return view('dashboard.jobs.edit', compact('job'));
    }


    public function update(Request $request, Job $job)
    {
        $validated_data = $request->validate([
            'title' => 'required|string',
            'type' => 'required|string',
            'category' => 'required|string',
            'location' => 'nullable|string',
            'lat' => 'nullable|string',
            'lng' => 'nullable|string',
            'map_zoom' => 'nullable|string',
            'salary_min' => 'nullable|string',
            'salary_max' => 'nullable|string',
            'tags' => 'nullable|string',
            'description' => 'required|string',
        ]);

        $validated_data["tags"] = explode(',', $validated_data["tags"]);

        $job->update($validated_data);

        return redirect()->route('dashboard.jobs.edit', $job->id)
            ->with('message', 'job updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Job $job
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Job $job)
    {
        try {
            Auth::user()->jobs()->findOrFail($job->id)->delete();
            return redirect()->back()->with('message', 'job deleted');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function tempAttachments(Request $request)
    {
        $user = User::find(Auth::id());
        try {
            $file = $user->addMediaFromRequest('file')
                ->toMediaCollection('tempJobsAttachments');
            return response()->json($file);
        } catch (FileDoesNotExist $e) {
        } catch (FileIsTooBig $e) {
        }
        return null;
    }

    public function addAttachments($id, Request $request)
    {
        $user = User::find(Auth::id());
        $job = $user->jobs->find($id);
        try {
            $file = $job->addMediaFromRequest('file')
                ->toMediaCollection('attachments');
            return response()->json($file);
        } catch (FileDoesNotExist $e) {
        } catch (FileIsTooBig $e) {
        }
        return null;
    }

    public function removeAttachment($uuid)
    {
        $media = Media::findByUuid($uuid);
        $user = User::find(Auth::id());

        try {
            if ($user->jobs->find($media->getAttribute('model_id')))
                if ($media->delete())
                    return response()->noContent();
            return 'nop!';
        } catch (\Exception $e) {
        }

        return null;
    }

    public function candidates(Job $job) {
        return view('dashboard.jobs.candidates.index', compact('job'));
    }

    public function showCandidates(Job $job, Candidate $candidate) {

        $user = $candidate->user;
        return view('dashboard.jobs.candidates.show', compact('job','candidate', 'user'));
    }

    public function deleteCandidates(Job $job, Candidate $candidate) {
        Auth::user()->jobs()->findOrFail($job->id);
        $job->candidates()->findOrFail($candidate->id);
        try {
            $candidate->delete();
            return redirect()->back()->with('message', 'candidate deleted');
        } catch (\Exception $e) {
        }
    }
}
