<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Job;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class UserController extends Controller
{
    public function account()
    {
        return view('dashboard.settings.account');
    }

    public function password()
    {
        return view('dashboard.settings.password');
    }

    public function update(Request $request)
    {
        $validated_data = $request->validate(['email' => ['sometimes', 'email', 'unique:users,name,' . Auth::id(), 'string'],
            'name' => 'required|string',
            'phone' => 'nullable|string',
            'skills' => 'nullable|string',
            'title' => 'nullable|string',
            'city' => 'nullable|string',
            'description' => 'nullable|string',
            'map_zoom' => 'nullable|string',
            'lat' => 'nullable|string',
            'lng' => 'nullable|string',
            'location' => 'nullable|string',
        ]);

        $user = User::find(Auth::id());

        $validated_data["skills"] = explode(',', $validated_data["skills"]);

        $user->update($validated_data);
        $user->save();

        if ($request->has('image'))
            try {
                $user->addMediaFromRequest('image')->toMediaCollection('images');
            } catch (FileDoesNotExist $e) {
            } catch (FileIsTooBig $e) {
            }

        if ($request->has('apply_to_job')) {
            $job = Job::find($request['apply_to_job']);
            if ($job)
                if (!$user->candidatesJobs()->find($job->id)) {
                    $candidate = Candidate::create([
                        'user_id' => Auth::id(),
                        'job_id' => $job->id
                    ]);

                    return redirect()->route('dashboard.candidates.edit', $candidate->id)
                        ->with('message', 'account information has been updated');
                } else {
                    $candidate = $user->candidates()->where('job_id', $job->id)->first();
                    return redirect()->route('dashboard.candidates.edit', $candidate->id)
                        ->with('message', 'account information has been updated');
                }
        }

        return redirect()->route('dashboard.settings')->with('message', 'account information has been updated');
    }

    public function updatePassword(Request $request)
    {
        $validated_data = $request->validate(['email' => ['sometimes', 'email', 'unique:users,name,' . Auth::id(), 'string'],
            'password' => 'required|string',
            'new_password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user = User::find(Auth::id());

        $user->password = Hash::make($validated_data['new_password']);
        $user->save();

        return redirect()->route('dashboard.settings.password')->with('message', 'password has been updated');
    }

    public function uploadAttachment(Request $request)
    {
        $user = User::find(Auth::id());
        try {
            $file = $user->addMediaFromRequest('file')
                ->toMediaCollection('attachments');
            return response()->json($file);
        } catch (FileDoesNotExist $e) {
        } catch (FileIsTooBig $e) {
        }

        return null;
    }

    public function removeAttachment($uuid)
    {
        $media = Media::findByUuid($uuid);

        try {
            if (Auth::id() == $media->getAttribute('model_id'))
                if ($media->delete())
                    return response()->noContent();
        } catch (\Exception $e) {
        }

        return null;
    }
}
