<?php

namespace App\Http\Middleware;

use Closure;

class checkAccountType
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param $type
     * @return mixed
     */
    public function handle($request, Closure $next, $type)
    {
        if ($type == 'employer' and !$request->user()->employer ) {
            return abort(403);
        }

        if ($type == 'freelancer' and !$request->user()->freelancer ) {
            return abort(403);
        }

        return $next($request);

    }
}
