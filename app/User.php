<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $phone
 * @property int $freelancer
 * @property int $employer
 * @property int $active
 * @property array|null $skills
 * @property string|null $title
 * @property string|null $location
 * @property string|null $description
 * @property float|null $lng
 * @property float|null $lat
 * @property int|null $map_zoom
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Bookmark[] $bookmarks
 * @property-read int|null $bookmarks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Candidate[] $candidates
 * @property-read int|null $candidates_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Job[] $candidatesJobs
 * @property-read int|null $candidates_jobs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Bookmark[] $companyBookmarks
 * @property-read int|null $company_bookmarks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Bookmark[] $freelancerBookmarks
 * @property-read int|null $freelancer_bookmarks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Job[] $jobs
 * @property-read int|null $jobs_count
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Candidate[] $usersCandidates
 * @property-read int|null $users_candidates_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmployer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFreelancer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMapZoom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSkills($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable implements HasMedia
{
    use Notifiable, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'freelancer',
        'employer', 'active', 'skills', 'title', 'description',
        'location', 'map_zoom', 'lat', 'lng'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'skills' => 'array',
        'lat' => 'double',
        'lng' => 'double',
        'map_zoom' => 'integer',
    ];

    public function image()
    {
        $media = $this->getMedia('images')->last();
        if ($media) return $media->getUrl('thumb');
        return asset('images/user-avatar-placeholder.png');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        try {
            $this->addMediaConversion('thumb')
                ->crop('crop-center', 150, 150);
        } catch (InvalidManipulation $e) {
        }

    }

    public function jobs(): HasMany
    {
        return $this->hasMany(Job::class);
    }

    public function candidates(): HasMany
    {
        return $this->hasMany(Candidate::class);
    }

    public function candidatesJobs(): BelongsToMany
    {
        return $this->belongsToMany(Job::class, 'candidates');
    }

    public function usersCandidates(): HasManyThrough
    {
        return $this->hasManyThrough(Candidate::class, Job::class);
    }

    public static function customSearch(array $params)
    {
        $query = User::getQuery();

        if (key_exists('lat', $params) and
            key_exists('lng', $params) and
            key_exists('distance', $params) and
            key_exists('location', $params) and
            $params['lat'] and
            $params['lng'] and
            $params['distance'] and
            $params['location']
        ) {
            $lat = $params['lat'];
            $lng = $params['lng'];
            $distance = $params['distance'];

            $query = $query->whereRaw('lat BETWEEN ( ? - ( ? * 0.0117)) AND ( ? + ( ? * 0.0117))',[$lat, $distance, $lat, $distance]);
            $query = $query->whereRaw('lng BETWEEN ( ? - ( ? * 0.0117)) AND ( ? + ( ? * 0.0117))', [$lng, $distance, $lng, $distance]);
        }

        if (key_exists('keywords',$params) and $params['keywords']) {
            $keywords = explode(',', $params['keywords']);
            if (count($keywords)) {
                $query = $query->whereRaw('MATCH (name, title ,description) AGAINST (?)', $keywords);
            }
        }

        //dd($query->toSql());

        return $query;
    }

    public function bookmarks(): HasMany
    {
        return $this->hasMany(Bookmark::class);
    }

    public function freelancerBookmarks(): HasMany
    {
        return $this->hasMany(Bookmark::class,'freelancer_id');
    }

    public function companyBookmarks(): HasMany
    {
        return $this->hasMany(Bookmark::class, 'company_id');
    }

    public function freelancerBookmark() {
        return $this->freelancerBookmarks()
            ->where('user_id', \Auth::id())
            ->limit(1)->first();
    }

    public function companyBookmark() {
        return $this->companyBookmarks()
            ->where('user_id', \Auth::id())
            ->limit(1)->first();
    }

}
