<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * App\Job
 *
 * @property int $id
 * @property string $title
 * @property string $type
 * @property string $category
 * @property string|null $location
 * @property float|null $lng
 * @property float|null $lat
 * @property int|null $map_zoom
 * @property int|null $salary_min
 * @property int|null $salary_max
 * @property array|null $tags
 * @property string $description
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Bookmark[] $bookmarks
 * @property-read int|null $bookmarks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Candidate[] $candidates
 * @property-read int|null $candidates_count
 * @property-read \App\User $company
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|Job newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Job newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Job query()
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereMapZoom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereSalaryMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereSalaryMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Job whereUserId($value)
 * @mixin \Eloquent
 */
class Job extends Model implements HasMedia
{

    use InteractsWithMedia;

    protected $fillable = [
        'title',
        'type',
        'category',
        'location',
        'lat',
        'lng',
        'map_zoom',
        'salary_min',
        'salary_max',
        'tags',
        'description',
        'user_id',
    ];

    protected $casts = [
        'tags' => 'array',
        'lat' => 'double',
        'lng' => 'double',
        'map_zoom' => 'integer'
    ];

    public function company(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function similarJobs()
    {
        return Job::where('id', '<>', $this->id)
            ->where('category', $this->category)
            ->limit(2)
            ->get();
    }

    public function candidates(): HasMany
    {
        return $this->hasMany(Candidate::class);
    }

    public function bookmarks(): HasMany
    {
        return $this->hasMany(Bookmark::class);
    }

    public function bookmark()
    {
        return $this->bookmarks()->where('user_id', \Auth::id())
            ->limit(1)->first();
    }

    public static function customSearchQuery(array $params)
    {
        $query = Job::getQuery();

        if (key_exists('lat', $params) and
            key_exists('lng', $params) and
            key_exists('distance', $params) and
            key_exists('location', $params) and
            $params['lat'] and
            $params['lng'] and
            $params['distance'] and
            $params['location']
        ) {
            $lat = $params['lat'];
            $lng = $params['lng'];
            $distance = $params['distance'];

            $query = $query->whereRaw('lat BETWEEN ( ? - ( ? * 0.0117)) AND ( ? + ( ? * 0.0117))', [$lat, $distance, $lat, $distance]);
            $query = $query->whereRaw('lng BETWEEN ( ? - ( ? * 0.0117)) AND ( ? + ( ? * 0.0117))', [$lng, $distance, $lng, $distance]);
        }

        if (key_exists('keywords', $params) and $params['keywords']) {
            $keywords = explode(',', $params['keywords']);
            if (count($keywords)) {
                $query = $query->whereRaw('MATCH (title ,description) AGAINST (?)', $keywords);
            }
        }

        if (key_exists('salary', $params)) {
            $salary = explode(',', $params['salary']);
            $query = $query
                ->where('salary_min', '<=', $salary[1])
                ->where('salary_max', '>=', $salary[0]);
        }

        if (key_exists('categories', $params)) {
            $categoriesSearch = $params['categories'];
            if (count($categoriesSearch)) {
                $query = $query->whereIn('category', $categoriesSearch);
            }
        }

        if (key_exists('types', $params)) {
            $typesSearch = $params['types'];
            if (count($typesSearch)) {
                $query = $query->whereIn('type', $typesSearch);
            }
        }

        return $query;
    }
}
