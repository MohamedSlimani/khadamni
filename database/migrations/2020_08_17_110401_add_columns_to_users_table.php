<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->nullable();
            $table->boolean('freelancer')->default(false);
            $table->boolean('employer')->default(false);
            $table->boolean('active')->default(false);
            $table->json('skills')->nullable();
            $table->string('title')->nullable();
            $table->string('location')->nullable();
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['phone', 'freelancer', 'employer', 'active', 'skills', 'title', 'city', 'description']);
        });
    }
}
