function initAutocomplete () {
    const input = document.getElementById('autocomplete-input')

    const lngInput = $('#lng')
    const latInput = $('#lat')
    const mapZoomInput = $('#map_zoom')
    const locationInput = $('#location')

    const options =  {
        zoom: parseInt(mapZoomInput.val()) || 6,
        center: {
            lat: parseFloat(latInput.val()) || 34.8471273,
            lng: parseFloat(lngInput.val()) || 1.5450766
        }
    }

    const map = new google.maps.Map(document.getElementById('map'), options)

    const autocomplete = new google.maps.places.Autocomplete(input, {
        // types: ['(cities)'],
        // componentRestrictions: {country: "us"}
    })

    map.addListener('center_changed', () => {
        latInput.val(map.getCenter().lat())
        lngInput.val(map.getCenter().lng())
    })
    map.addListener('zoom_changed',() => {
        mapZoomInput.val(map.getZoom())
    })

    autocomplete.addListener('place_changed', () => {
        locationInput.val(autocomplete.getPlace().formatted_address)
        map.setCenter({
            lat: autocomplete.getPlace().geometry.location.lat(),
            lng: autocomplete.getPlace().geometry.location.lng()
        })
    })
}

const descriptionInput = $('#description-input')

const editor = pell.init({
    element: document.getElementById('description-editor'),
    onChange: html => {
        descriptionInput.val(he.encode(html))
    },
    actions: [
        'bold',
        'italic',
        'underline',
        'strikethrough',
        'heading1',
        'heading2',
        'paragraph',
        'quote',
        'olist',
        'ulist',
        'code',
        'line',
        'link',
        'image',
    ],
})

editor.content.innerHTML = descriptionText
descriptionInput.val(he.encode(descriptionText))
