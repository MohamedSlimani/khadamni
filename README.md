# Khademni
# Memeoire fin etude

# PAR: **Halima Slimani** et **Morjani Bakhta** et **Rabouh Rosa **

# Encadre Par:** Mr KHADHIR Bekki  **

2021

## installation

```bash

git clone 

composer install

cp .env.example .env

# fill in the database info

php artisan key:gen

php artisan migrate

php artisan serve

```
